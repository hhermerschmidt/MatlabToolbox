function [eleConnect_t0, eleConnect_t1] = ConnectivityChangeSSMUM(eleConnect_t0, eleConnect_t1, sslConnect, sslIntNodes)
%% Function performing reconnection SSMUM procedure.
%
% Input Arguments
% ---------------
%

% 
% eleConnect_t0: nEle x 4 float matrix with nodeIDs
%                at beginning of time slab
%                [ nodeA | nodeB | nodeC | nodeD ]
%
% eleConnect_t1: nEle x 4 float matrix with nodeIDs
%                at end of time slab
%                [ nodeA | nodeB | nodeC | nodeD ]
%
% sslConnect: nEleSSL x 5 float matrix with element and nodeIDs
%             [ elementID | nodeA | nodeB | nodeC | nodeD ]
%
% sslIntNodes: nIntNodesSSL x 1 float vector with nodeIDs of
%              interior, moving nodes of SSL
%
%
% Return Values
% -------------
%
% eleConnect_t0: nEle x 4 float matrix with reconnected nodeIDs
%                at beginning of time slab
%                [ nodeA | nodeB | nodeC | nodeD ]
%
% eleConnect_t1: nEle x 4 float matrix with reconnectednodeIDs
%                at end of time slab
%                [ nodeA | nodeB | nodeC | nodeD ]
%
%
% Usage
% -----
%
% [eleConnect_t0, eleConnect_t1] = ConnectivityChangeSSMUM(eleConnect_t0, eleConnect_t1, sslConnect, sslIntNodes)

% created by         Henning Schippke
% created on         18.04.2017 (based on script created in 07/2014)
% last modifified on 19.04.2017

% ======================================================================= %
% ======================================================================= %


NodesSSMUMLayer = sslIntNodes;

for mm = 1:size(sslConnect(:,1),1) % Elemente des shear-slip layers

    eleConnectSSMUM_old = eleConnect_t1(sslConnect(mm,1),:);

    nodePos1_new = find( NodesSSMUMLayer == eleConnectSSMUM_old(1) );
    nodePos2_new = find( NodesSSMUMLayer == eleConnectSSMUM_old(4) );

    nodePos1_new = nodePos1_new - 1;
    nodePos2_new = nodePos2_new - 1;

    if nodePos1_new == 0
        nodePos1_new = length(NodesSSMUMLayer);
    end

    if nodePos2_new == 0
        nodePos2_new = length(NodesSSMUMLayer);
    end

    nodeNumb1_new = NodesSSMUMLayer( nodePos1_new );
    nodeNumb2_new = NodesSSMUMLayer( nodePos2_new );

    eleConnect_t0(sslConnect(mm,1),:) = [ nodeNumb1_new eleConnectSSMUM_old(2) eleConnectSSMUM_old(3) nodeNumb2_new ];
    eleConnect_t1(sslConnect(mm,1),:) = eleConnect_t0(sslConnect(mm,1),:);

    %elem{mm}.SetEleNodes(eleConnect_t0 , eleConnect_t1);

end %for
       
end %function "ConnectivityChangeSSMUM"