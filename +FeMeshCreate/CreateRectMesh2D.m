function [coord, connect] = CreateRectMesh2D( domain, ele )
% This function generates a 2D finite element mesh with node coordinates
% <coord> and connectivity <connect> using 4 node quadrilaterals.
%
% Input data:
% --------------
% domain : 1 x 4 matrix with corner points of the domain
%          [ xMin, xMax, yMin, yMax ]
%
% ele    : 1 x 2 matrix with number of elements in x and y direction
%          [nEleX, nEleY ]
%
% Output data:
% ---------------
% coord   :  nNode x 2 matrix with node coordinates
%            [ xCoord | yCoord ]
%
% connect :  nEle x 4 matrix containing the element connectivity 
%            [ nodeA | nodeB | nodeC | nodeD ]
%
% Usage:
% ---------
% [coord, connect] = CreateRectMesh2D( domain, ele )
  
% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %

% Read input data
xMin = domain(1);
xMax = domain(2);

yMin = domain(3);
yMax = domain(4);

nEleX = ele(1);
nEleY = ele(2);


% Calculate numbers of nodes and elements
nNodes =  (nEleX + 1) * (nEleY +1);
nEle   =  nEleX * nEleY;


% Get the number of nodes in each direction for a mesh
% with quadratic shape functions:   
nNodesX  = nEleX + 1;
nNodesY  = nEleY + 1;


% Node coordinates: 
posX = linspace(xMin, xMax, nNodesX)';
posY = linspace(yMin, yMax, nNodesY)';


% Set node coordinates:
%--------------------------------
coord = zeros(nNodes, 2); % Initialisation

c = 1;
for i = 1 : nNodesX
    for j = 1 : nNodesY     
        coord(c,:) = [posX(i), posY(j)];
        c = c + 1;
    end
end


% Set connectivity of elements with 4 nodes each:
%-------------------------------------------------
connect = zeros(nEle, 4); % Initialisation

% Get the number of elements in each direction:
nx = nEleX;
ny = nEleY;

currElem = 1;
for k = 1 : nx
    for m = 1 : ny
        % Initialization:
        currInzi = zeros(1,4);

        % Get global number for local node 1:
        currInzi(1) = (k-1)*(ny+1) + (m-1) + 1;

        % Get further global nodes:
        currInzi(2) = currInzi(1) + (ny+1);
        currInzi(3) = currInzi(2) + 1;
        currInzi(4) = currInzi(1) + 1;

        connect(currElem, :) = currInzi;
        currElem = currElem + 1;
    end
end
                    
                    
                
end
