function [ elementVtkIdTable ] = MapElementNumber2vtkCellId( connect )
% Description:
% ---------------
% Function creating a look up table elementVtkIdTable which maps/translates
% element numbers to vtk cell ids and vice versa. The vtk cell ids range
% continuously from 0 to (nEle-1).
%
% Input data:
% --------------
% connect : nEle x 3 cell
%           { [ elementNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ elementTypeId ] }
%
% Output data:
% ---------------
% elementVtkIdTable : nEle x 3 matrix
%                   [ elementNumber | vtkCellId | elementTypeId ]
%
% Usage:
% ---------
% [ elementVtkIdTable ] = MapElementNumber2vtkCellId( connect )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... mapping element numbers to vtk cell  ids ...');

nEle = size(connect,1);

elementVtkIdTable = zeros(nEle,3);
cc = 0;
for mm = 1:nEle
        
    elementVtkIdTable(mm,1) = connect{mm,1}; % element number
    
    elementVtkIdTable(mm,2) = cc;            % vtk cell id
    cc = cc + 1;
 
    elementVtkIdTable(mm,3) = connect{mm,3}; % element type id
    
end %for
    
end %function

% ======================================================================= %
% ======================================================================= %