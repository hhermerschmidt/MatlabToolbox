function [ indent ] = CreateIndentDictionary( indentString, maxIndentLevel )
% Function creating a dictionary with different indentation level strings.
%
% Input data:
% --------------
% indent: string with indentation string, e.g. '    '
%
% maxIndentLevel: integer specifying maximum indentation level
%
% Output data:
% ---------------
% indent: dictionary with indent strings
%         indent('1') = '    '
%         indent('2') = '        '
%         indent('3') = '            '
%
% Usage:
% ---------
% [ indent ] = CreateIndentDictionary( indentString, maxIndentLevel )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


indent      = containers.Map;

indent('1') = indentString;

for ii = 2:maxIndentLevel
    indent(num2str(ii)) = [indent(num2str(ii-1)),indentString];
end %for


end %function

% ======================================================================= %
% ======================================================================= %