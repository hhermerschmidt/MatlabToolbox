function [ pointsInCell ] = CreateLutVtkCellPoints()
% Description:
% ---------------
% Function creating a look-up-table (LUT) for getting the number of vtk
% points per vtk cell via the vtk cell type id.
%
% Input data:
% --------------
% [] : None
%
% Output data:
% ---------------
% pointsInCell : nVtkCellType x 2 matrix
%                [ vtkCellTypeID | number of points per cell ]
%
% Usage:
% ---------
% [ pointsInCell ] = CreateLutVtkCellPoints()

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


%              [ vtkCellTypeID, number of points per cell ]
pointsInCell = [     1       ,            1               ; ... 
                     3       ,            2               ; ...
                     9       ,            4               ]; 

end

% ======================================================================= %
% ======================================================================= %