function [] = WritePointCoordinates(fid, indent, vtkPointData)
% Description:
% ---------------
% Function writing point coordinates to vtk-file in xml-style.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkPointData : nNode x 4 matrix
%                [ vtkPointId | xCoord | yCoord | zCoord ]
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WritePointCoordinates(fid, indent, coord_vtk)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... writing point coordinates  to vtk file ...');


fprintf(fid, [indent('3'), '<Points>\n']);


dataArrayName   = 'coordinates';
dataArrayType   = 'Float32';
dataArrayNComp  = '3';
dataArrayFormat = 'ascii';

fprintf(fid, [indent('4'), '<DataArray type="'              ,dataArrayType  ,'" '...
                                      'Name="'              ,dataArrayName  ,'" '...
                                      'NumberOfComponents="',dataArrayNComp ,'" '...
                                      'format="'            ,dataArrayFormat,'"' ...
                           '>\n']);

                       
nVtkPoints = size(vtkPointData,1);

for nn = 1:nVtkPoints
    % hier absichtlich kein separates Herauslesen der vtkPointId
    fprintf(fid, [indent('5'), '%6.8f %6.8f %6.8f\n'], vtkPointData(nn,2:end));
end %for

fprintf(fid, [indent('4'), '</DataArray>\n']);

fprintf(fid, [indent('3'), '</Points>\n']);

end %function

% ======================================================================= %
% ======================================================================= %