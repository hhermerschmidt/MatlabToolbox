function [ ] = WriteVtkFile(vtkFileData            , ...
                            coord       , connect  , ...
                            nodeData    , elementData, parallelExecution)
% Description:
% ---------------
% Function creating a vtu-file (vtk data format for unstructured grid data)
% from given node coordinates <coord> and connectivity <connect>.
% In case additional node  data <nodeData> and element data <elementData>
% is given, these data is saved in the vtu-file as vtk point and cell data fields.
%
% Input data:
% --------------
% vtkFileData : control data structure
%     .outputDataFolder : string containing the path to the folder where
%                         the vtu-file will be stored
%     .outputFileName   : file name of vtu-file
%     .flag
%         .writePointData : flag for writing node data as point data to
%                           vtu-file
%                           0 : no, 1: yes
%         .writeCellData  : flag for writing element data as cell data to
%                           vtu-file
%                           0 : no, 1: yes
%
% coord   : nNode x 4 matrix
%           [ nodeNumber | xCoord | yCoord | zCoord ]
%
% connect : nEle x 3 cell
%           { [ elementNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ elementTypeId ] }
%
% nodeData : nNodeData x 1 structured array
%     .fieldName  : name of point data field
%     .dataType   : data type : (U)Int8/16/32/64, Float32/64
%     .dataFormat : data format string, e.g. %8.4f
%     .nComp      : number of components per point
%     .data       : point data array
%
% elementData : nEleData x 1 structured array
%     .fieldName  : name of cell data field
%     .dataType   : data type : (U)Int8/16/32/64, Float32/64
%     .dataFormat : data format string, e.g. %8.4f
%     .nComp      : number of components per cell
%     .data       : cell data array
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteVtkFile(vtkFileData, coord, connect, nodeData, elementData, parallelExecution)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 26.01.17

% TO DO:
% - transferring bundle of functions in module csv2vtk into one single
%   class (better done using python).
% - transferring Matlab package csv2vtk into a python class.
% - extending the function WriteXmlFile such that it is able to write the
%   imported csv-data to different vtk-file formats (rectlinear grid,
%   structured grid, polyData); if necessary (?)

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('# -------------------------------------------------- #');
display('# Parsing data to vtk file format ... begin          #');
display('# -------------------------------------------------- #');

% import package
import vtk.WriteVtkFile.*

% -- Creating vtkIdTables ----------------------------------------------- %

% map/translate node number <--> vtk point id
nodeVtkIdTable    = MapNodeNumber2vtkPointId(coord);

% map/translate element number <--> vtk cell id
elementVtkIdTable = MapElementNumber2vtkCellId(connect);

% ----------------------------------------------------------------------- %


% -- Creating vtk point and cell data ----------------------------------- %

% create vtk point coordinate data
vtkCoord   = CreateVtkPointData(coord, nodeVtkIdTable, parallelExecution);

% create vtk cell connectivity data
vtkConnect = CreateVtkCellData(connect, elementVtkIdTable, nodeVtkIdTable, parallelExecution);

% ----------------------------------------------------------------------- %


% Extending control data structure -------------------------------------- %
% Control data structure is extended with default values...

vtkFileData.dataType      = 'UnstructuredGrid';
vtkFileData.fileExtension = 'vtu';

vtkFileData.version       = '0.1';
vtkFileData.byteOrder     = 'LittleEndian';

% ... and with vtk grid related data.
vtkFileData.nVtkPoints    = size(vtkCoord  , 1);
vtkFileData.nVtkCells     = size(vtkConnect, 1);

% ----------------------------------------------------------------------- %


% -- Write vtk-file ----------------------------------------------------- %

% create indent dictionary
indent = CreateIndentDictionary('    ', 8);


fid = OpenVtkFile(indent, vtkFileData);


if vtkFileData.flag.writePointData == 1
    WritePointData(fid, indent, vtkCoord, nodeVtkIdTable, nodeData);
end

if vtkFileData.flag.writeCellData == 1
    WriteCellData(fid, indent, vtkConnect, elementVtkIdTable, elementData);
end


WritePointCoordinates(fid, indent, vtkCoord);

WriteCellConnectivity(fid, indent, vtkConnect)


CloseVtkFile(fid, indent, vtkFileData);


display('# -------------------------------------------------- #');
display('# Parsing data to vtk file format ... end            #');
display('# -------------------------------------------------- #');

% ----------------------------------------------------------------------- %

end %function WriteXmlFile

% ======================================================================= %
% ======================================================================= %
