function [ nodeVtkIdTable ] = MapNodeNumber2vtkPointId( coord )
% Description:
% ---------------
% Function creating a look up table nodeVtkIdTable which maps/translates
% node numbers to vtk point ids and vice versa. The vtk point ids range
% continuously from 0 to (nNode-1).
%
% Input data:
% --------------
% coord   : nNode x 4 matrix
%           [ nodeNumber | xCoord | yCoord | zCoord ]
%
% Output data:
% ---------------
% nodeVtkIdTable : nNode x 2 matrix
%                  [ nodeNumber | vtkPointId ]
%
% Usage:
% ---------
% [ nodeVtkIdTable ] = MapNodeNumber2vtkPointId( coord )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... mapping node    numbers to vtk point ids ...');

nNode = size(coord,1);

nodeVtkIdTable      = zeros(nNode,2);

nodeVtkIdTable(:,1) = coord(:,1);    % node numbers
nodeVtkIdTable(:,2) = 0:nNode-1;     % vtk point id

end

% ======================================================================= %
% ======================================================================= %