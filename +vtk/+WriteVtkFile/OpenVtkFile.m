function [ fid ] = OpenVtkFile(indent, vtkFileData)
% Description:
% ---------------
% Function creating and opening a vtk-file and writing the header lines of
% that file in xml-style.
%
% Input data:
% --------------
% indent: dictionary with indent strings
%
% vtkFileData : control data structure
%     .outputDataFolder : string containing the path to the folder where
%                         the vtu-file will be stored
%     .outputFileName   : file name of vtu-file
%     .flag
%         .writePointData : flag for writing node data as point data to
%                           vtu-file
%                           0 : no, 1: yes
%         .writeCellData  : flag for writing element data as cell data to
%                           vtu-file
%                           0 : no, 1: yes
%     .dataType      : vtk data type, e.g. 'Unstructured Grid'
%     .fileExtension : vtk file extension, e.g. 'vtu'
%     .version       : vtk version, e.g. '0.1'
%     .byteOrder     : machine byte ordering ('LittleEndian', BigEndian')
%     .nVtkPoints    : number of vtk points
%     .nVtkCells     : number of vtk cells
%
% Output data:
% ---------------
% fid : file id
%
% Usage:
% ---------
% [ fid ] = OpenVtkFile(indent, vtkFileData)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 17.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... opening vtk file ...');


% extract vtk file data
vtkOutputDataFolder = vtkFileData.outputDataFolder;
vtkOutputFileName   = vtkFileData.outputFileName;

vtkDataType         = vtkFileData.dataType;
vtkFileExtension    = vtkFileData.fileExtension;

vtkVersion          = vtkFileData.version;
vtkByteOrder        = vtkFileData.byteOrder;

nVtkPoints          = vtkFileData.nVtkPoints;
nVtkCells           = vtkFileData.nVtkCells;

BasicUtilities.CreateFolder(vtkOutputDataFolder,[]);

fid = fopen([vtkOutputDataFolder,vtkOutputFileName,'.',vtkFileExtension],'w');


fprintf(fid, ['<VTKFile type='      ,'"',vtkDataType ,'" ',...
                       'version='   ,'"',vtkVersion  ,'" ',...
                       'byte_order=','"',vtkByteOrder,'"' ,...
              '>\n']);

fprintf(fid, [indent('1'), '<',vtkDataType,'>\n']);
fprintf(fid, [indent('2'), '<Piece ',...
                                'NumberOfPoints=','"',num2str(nVtkPoints),'" ',...
                                'NumberOfCells=' ,'"',num2str(nVtkCells) ,'"' ,...
                           '>\n']);

end

% ======================================================================= %