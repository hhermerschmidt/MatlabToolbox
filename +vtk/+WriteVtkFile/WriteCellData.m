function [] = WriteCellData(fid, indent, vtkConnect, elementVtkIdTable, varargin)
% Description:
% ---------------
% Function writing cell data to vtk-file.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkConnect : nVtkCell x 3 cell array
%              { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% elementVtkIdTable : nEle x 3 matrix
%                   [ elementNumber | vtkCellId | elementTypeId ]
%
% varargin{1} = elementData : nElementData x 1 structured array
%     elementData(ii).fieldName  : name of element data field
%     elementData(ii).dataType   : data type : (U)Int8/16/32/64, Float32/64 
%     elementData(ii).dataFormat : data format string, e.g. %8.4f
%     elementData(ii).nComp      : number of components per cell
%     elementData(ii).data       : vtk cell data array
%                                  nVtkCell x 2 matrix
%                                  [ elementNumber | dataValue ]
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteCellData(fid, indent, vtkConnect, elementVtkIdTable, varargin)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... writing cell  data         to vtk file ...');

fprintf(fid, [indent('3'), '<CellData Scalars="elementNumber">\n']);

% -- Write element numbers as node data field --------------------------- %

elementData.fieldName  = 'elementNumber';
elementData.dataType   = 'Float32';
elementData.dataFormat = '%8.4f';
elementData.nComp      = '1';
elementData.data       = [elementVtkIdTable(:,1), elementVtkIdTable(:,1)];

WriteCellData_Scalars(fid, indent, vtkConnect, elementVtkIdTable, elementData);

% ----------------------------------------------------------------------- %


% -- Write further element data fields ---------------------------------- %

if ~isempty(varargin)
    
    elementData = varargin{1};

    for ii = 1:length(elementData)
        WriteCellData_Scalars(fid, indent, vtkConnect, elementVtkIdTable, elementData(ii));
    end
    
end %if

% ----------------------------------------------------------------------- %


fprintf(fid, [indent('3'), '</CellData>\n']);

end %function

% ======================================================================= %


% ======================================================================= %
% Subfunction
% ======================================================================= %

function [] = WriteCellData_Scalars(fid, indent, inziConnect_vtk, elementVtkIdTable, elementData)
% Function writing scalar element data to vtk-file (only 1 value per point,
% i.e. nodeData(ii).nComp = 1.

% get field data
fieldName  = elementData.fieldName;
dataType   = elementData.dataType;
dataFormat = elementData.dataFormat;
nComp      = elementData.nComp;
data       = elementData.data;
format     = 'ascii';

fprintf(fid, [indent('4'), '<DataArray type="'              ,dataType  ,'" ', ...
                                      'Name="'              ,fieldName ,'" ', ...
                                      'NumberOfComponents="',nComp     ,'" ', ...
                                      'format="'            ,format    ,'"' , ...
                            '>\n']);                      

% loop over all vtk cells and write corresponding data
nVtkCells = size(inziConnect_vtk,1);

for mm = 1:nVtkCells
    
    vtkCellId        = inziConnect_vtk{mm,1};
    elementNumber    = elementVtkIdTable(elementVtkIdTable(:,2) == vtkCellId, 1);    
    
    elementDataValue = data( data(:,1) == elementNumber, 2);    
    
    fprintf(fid, [indent('5'), dataFormat,'\n'], elementDataValue);
    
end


fprintf(fid, [indent('4'), '</DataArray>\n']);

end %function

% ======================================================================= %

% ======================================================================= %
% ======================================================================= %