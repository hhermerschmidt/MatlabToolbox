function [ vtkCellData ] = CreateVtkCellData( connect, elementVtkIdTable, nodeVtkIdTable, parallelExecution )
% Description:
% ---------------
% Function creating a cell array with vtk cell ids, vtk point connectivity
% and element type id from given connectivity.
%
% Input data:
% --------------
% connect : nEle x 3 cell
%           { [ elementNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ elementTypeId ] }
%
% elementVtkIdTable : nEle x 3 matrix
%                   [ elementNumber | vtkCellId | elementTypeId ]
%
% nodeVtkIdTable : nNode x 2 matrix
%                  [ nodeNumber | vtkPointId ]
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% vtkCellData : nVtkCell x 3 cell array
%               { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% Usage:
% ---------
% [ vtkCellData ] = CreateVtkCellData( connect, elementVtkIdTable, nodeVtkIdTable, parallelExecution )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 26.01.17

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... create  vtk cell  connectivity array ...');

% import package
import vtk.WriteVtkFile.*


% Get mapping of element type id <--> vtk cell type id
elementType2vtkCellType = CreateLutEleTypeId2vtkCellTypeId();

% Bemerkung:
% Mit Hilfe der Zuordnung elementTypeId <--> vtkCellTypeId kann (wenn
% notwendig) auch noch eine Umsortierung der Knotenreihenfolge auf
% Elementebene erfolgen bzw. bei Quad9- und quad. 3D-Elementen das
% Herausfiltern von Flaechen- und Volumenmittelknoten.


nEle        = size(connect,1);
vtkCellData = cell(size(connect));

if parallelExecution == 1

    parfor mm = 1:nEle

        [elementNumber, connectivity, elementTypeId] = connect{mm,:};

        elementVtkId      = elementVtkIdTable(elementVtkIdTable(:,1) == elementNumber, 2);

        % translate node numbers (of element connectivity) to vtk point ids
        connectivity_vtk = zeros(size(connectivity));

        for ii = 1:length(connectivity) % loop over every node in element connectivity

            nodeNumber           = connectivity(ii);
            vtkPointId           = nodeVtkIdTable(nodeVtkIdTable(:,1) == nodeNumber, 2);
            connectivity_vtk(ii) = vtkPointId;

        end

        % translate element type id to vtk cell type id
        vtkCellTypeId     = elementType2vtkCellType(elementType2vtkCellType(:,1) == elementTypeId, 2);


        vtkCellData(mm,:) = {elementVtkId, connectivity_vtk, vtkCellTypeId};

    end %for

    try
        warning('off','parallel:lang:matlabpool:CloseDeprecation')
        matlabpool close
    catch
        delete(gcp('nocreate'))
    end

else

    for mm = 1:nEle

        [elementNumber, connectivity, elementTypeId] = connect{mm,:};

        elementVtkId      = elementVtkIdTable(elementVtkIdTable(:,1) == elementNumber, 2);

        % translate node numbers (of element connectivity) to vtk point ids
        connectivity_vtk = zeros(size(connectivity));

        for ii = 1:length(connectivity) % loop over every node in element connectivity

            nodeNumber           = connectivity(ii);
            vtkPointId           = nodeVtkIdTable(nodeVtkIdTable(:,1) == nodeNumber, 2);
            connectivity_vtk(ii) = vtkPointId;

        end

        % translate element type id to vtk cell type id
        vtkCellTypeId     = elementType2vtkCellType(elementType2vtkCellType(:,1) == elementTypeId, 2);


        vtkCellData(mm,:) = {elementVtkId, connectivity_vtk, vtkCellTypeId};

    end %for

end %if

end %function

% ======================================================================= %
% ======================================================================= %
