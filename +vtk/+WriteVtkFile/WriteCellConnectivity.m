function [] = WriteCellConnectivity(fid, indent, vtkCellData)
% Description:
% ---------------
% Function writing vtk cell connectivity data to vtk-file in xml-style.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkCellData : nVtkCell x 3 cell array
%               { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% elementVtkIdTable : nEle x 3 matrix
%                   [ elementNumber | vtkCellId | elementTypeId ]
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteCellConnectivity(fid, indent, vtkCellData, elementVtkIdTable)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... writing cell  connectivity to vtk file ...');


fprintf(fid, [indent('3'), '<Cells>\n']);

% connectivity
WriteCellConnectivity_Connectivity(fid, indent, vtkCellData);

% offset
WriteCellConnectivity_Offset(fid, indent, vtkCellData);

% vtk cell type id 
WriteCellConnectivity_CellTypeId(fid, indent, vtkCellData);


fprintf(fid, [indent('3'), '</Cells>\n']);


end %function 

% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [] = WriteCellConnectivity_Connectivity(fid, indent, vtkConnect)
% Description:
% ---------------
% Function writing vtk cell connectivity.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkConnect : nVtkCell x 3 cell array
%              { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteCellConnectivity_Connectivity(fid, indent, vtkConnect)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


fprintf(fid, [indent('4'), '<DataArray type="Int32" Name="connectivity" format="ascii">\n']);

nVtkCells = size(vtkConnect,1);

for mm = 1:nVtkCells
    % hier absichtlich kein separates Herauslesen der vtkCellId, sondern
    % sukzessives Durchschreiten des cell arrays vtkConnect.
    
    % Bemerkung:
    % Mit Hilfe der Zuordnung elementTypeId <--> vtkCellTypeId kann (wenn
    % notwendig) auch noch eine Umsortierung der Knotenreihenfolge auf
    % Elementebene erfolgen bzw. bei Quad9- und quad. 3D-Elementen das
    % Herausfiltern von Flaechen- und Volumenmittelknoten.
    
    dataFormat = '%8i';
    cellConnectivity = vtkConnect{mm,2};
    nPointsPerCell   = length(cellConnectivity);
    
    dataFormatString  = ' ';
    for ii = 1:nPointsPerCell
        dataFormatString = [dataFormatString, dataFormat]; %#ok<AGROW>
    end
        
    fprintf(fid,[indent('5'), dataFormatString,'\n'], cellConnectivity);  
end


fprintf(fid, [indent('4'), '</DataArray>\n']); 

end

% ======================================================================= %


function [] = WriteCellConnectivity_Offset(fid, indent, vtkConnect)
% Description:
% ---------------
% Function writing offset of vtk cell.
% The offset of a vtk cell is a number which is related to the vtk cell
% connectivity data array. The offset number of one vtk cell specifies the
% position of the last vtk point (of the current vtk cell) in the vtk cell
% data array. Example: Each cell has 4 points, hence, the offset of cell 0
% is 4, offset of cell 1 is 8, ...
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkConnect : nVtkCell x 3 cell array
%              { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteCellConnectivity_Offset(fid, indent, vtkConnect)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package
import vtk.WriteVtkFile.*


% -- Get LookUpTable ---------------------------------------------------- %

% number of points per vtk cell
pointsPerCell = CreateLutVtkCellPoints();

% ----------------------------------------------------------------------- %


fprintf(fid, [indent('4'), '<DataArray type="Int32" Name="offsets" format="ascii">\n']); 

cc        = 0;                  % initial offset
nVtkCells = size(vtkConnect,1); % number of vtk cells

for mm = 1:nVtkCells % loop over all vtk cells
       
    vtkCellTypeID = vtkConnect{mm,3};   
    vtkCellOffset = pointsPerCell( pointsPerCell(:,1) == vtkCellTypeID, 2);   
    
    cc            = cc + vtkCellOffset; % offset of current vtk cell
    
    fprintf(fid, [indent('5'), '%6i\n'], cc);

end


fprintf(fid, [indent('4'), '</DataArray>\n']); 

end

% ======================================================================= %


function [] = WriteCellConnectivity_CellTypeId(fid, indent, vtkConnect)
% Description:
% ---------------
% Function writing vtk cell type id.
%
% Input data:
% --------------
% fid: file id
%
% indent: dictionary with indent strings
%
% vtkConnect : nVtkCell x 3 cell array
%              { [ vtkCellNumber ] | [ connectivity as 1 x nNodePerEle matrix ] | [ vtkCellTypeId ] }
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = WriteCellConnectivity_CellTypeId(fid, indent, vtkConnect)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ======================================================================= %
% ======================================================================= %


fprintf(fid, [indent('4'), '<DataArray type="Int32" Name="types" format="ascii">\n']); 

nVtkCells = size(vtkConnect,1);

for mm = 1:nVtkCells
    
    vtkCellTypeID = vtkConnect{mm,3};   

    fprintf(fid, [indent('5'), '%2i \n'], vtkCellTypeID);
    
end


fprintf(fid, [indent('4'), '</DataArray>\n']); 

end

% ======================================================================= %

% ======================================================================= %
% ======================================================================= %