function [ eleTyp2vtkCellType ] = CreateLutEleTypeId2vtkCellTypeId()
% Description:
% ---------------
% Function a look-up-table (LUT) for mapping/translating the element type
% id to the corresponding vtk cell type id and vice versa.
%
% Input data:
% --------------
% [] : None
%
% Output data:
% ---------------
% eleTyp2vtkCellType : (nEleType x nVtkCellType) x 2 matrix
%                      [ elementTypeID | vtkCellTypeID ]
%
% Usage:
% ---------
% [ eleTyp2vtkCellType ] = CreateLutEleTypeId2vtkCellTypeId()

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 20.04.15

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


%                    [ elementTypeID, vtkCellTypeID ]
eleTyp2vtkCellType = [       1      ,       1       ; ... 
                           105      ,       3       ; ...
                          4200      ,       9       ; ...
                          4251      ,       9       ];  


end

% ======================================================================= %
% ======================================================================= %