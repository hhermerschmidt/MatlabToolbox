function [ vtkPointData ] = CreateVtkPointData( coord, nodeVtkIdTable, parallelExecution )
% Description:
% ---------------
% Function creating a matrix with vtk point ids and corresponding
% coordinates from given nodes.
%
% Input data:
% --------------
% coord   : nNode x 4 matrix
%           [ nodeNumber | xCoord | yCoord | zCoord ]
%
% nodeVtkIdTable : nNode x 2 matrix
%                  [ nodeNumber | vtkPointId ]
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% vtkPointData : nNode x 4 matrix
%               [ vtkPointId | xCoord | yCoord | zCoord ]
%
% Usage:
% ---------
% [ vtkPointData ] = CreateVtkPointData( coord, nodeVtkIdTable, parallelExecution )

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 26.01.17

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


display('... create  vtk point coordinate   array ...');

nNodes       = size(coord,1);
vtkPointData = zeros(size(coord));

if parallelExecution == 1

    try
        warning('off','parallel:lang:matlabpool:OpenDeprecation');
        matlabpool open
    catch
        parpool;
    end

    parfor nn = 1:nNodes

        currCoord = coord(nn,:);
        currNodeNumber   = currCoord(1);
        currCoordinates  = currCoord(2:end);

        % translate node number to vtk point id
        nodeVtkId = nodeVtkIdTable(nodeVtkIdTable(:,1) == currNodeNumber, 2);

        vtkPointData(nn,:) = [nodeVtkId, currCoordinates];

    end %for

else
    
    for nn = 1:nNodes

        currCoord = coord(nn,:);
        currNodeNumber   = currCoord(1);
        currCoordinates  = currCoord(2:end);

        % translate node number to vtk point id
        nodeVtkId = nodeVtkIdTable(nodeVtkIdTable(:,1) == currNodeNumber, 2);

        vtkPointData(nn,:) = [nodeVtkId, currCoordinates];
    end %for

end %if

end %function

% ======================================================================= %
% ======================================================================= %
