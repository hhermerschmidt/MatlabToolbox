function [] = ParseCsv2vtkMeshData(csvDataFolder, vtkFileData, parallelExecution)
% Description:
% ---------------
% Function creating a vtk-file from csv data files for representing finite
% elements in Paraview. The node numbers are displayed as vtk point data field,
% whereas element number, material id and load id are displayed as vtk cell
% data fields.
%
% Input data:
% --------------
% csvDataFolder :  string containing path to folder with csv-files
%
% vtkFileData : structure with output path data and flags
%     .outputDataFolder : string with path to ouput folder
%     .outputFileName   : string with file name to created (without extension)
%     .flag
%          .writePointData : integer specifying if point data is stored in
%                            vtk-file
%                            0: no, 1:yes
%          .writeCellData  : integer specifying if cell data is stored in
%                            vtk-file
%                            0: no, 1:yes
%
% parallelExecution: boolean specifying, if vtk file creation will be performed in parallel
%                    (using parpool/matlabpool)
%
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = ParseCsv2vtk_meshData(csvDataFolder, vtkFileData, parallelExecution)

% created by   : Henning Schippke
% created on   : 10.04.15
% last modified: 26.01.17

% TO DO:
% - ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Read csv data files ------------------------------------------------ %

import FEM2FlowUtilities.CsvInputDataFiles.LoadCsvData

coord        = LoadCsvData(csvDataFolder, 'nodeCoordinates.csv' );
connectMat   = LoadCsvData(csvDataFolder, 'nodeConnectivity.csv');
elementTable = LoadCsvData(csvDataFolder, 'elementTable.csv'    );

% ----------------------------------------------------------------------- %


% -- Rearrange connectivity --------------------------------------------- %

% change connectivity data from matrix to cell type
connect = FEM2FlowUtilities.Connectivity.CreateConnectivityCellExt(connectMat);

% extend connectivity with elementTypeID
connectExt = cell(size(connect,1),3);

for mm = 1:size(connect,1)

    elementNumber    = connect{mm,1};
    elementTypeId    = elementTable( elementTable(:,1) == elementNumber, 2);

    connectExt{mm,1} = connect{mm,1};
    connectExt{mm,2} = connect{mm,2};
    connectExt{mm,3} = elementTypeId;

end

% ----------------------------------------------------------------------- %


% -- Node data fields --------------------------------------------------- %

nodeData = [];
% nodeData(1).fieldName  = '';
% nodeData(1).dataType   = 'Float32';
% nodeData(1).dataFormat = '%8.4f';
% nodeData(1).nComp      = '1';
% nodeData(1).data       = [];

% ----------------------------------------------------------------------- %


% -- Element data fields ------------------------------------------------ %

% element type ID
elementData(1).fieldName  = 'elementTypeID';
elementData(1).dataType   = 'Float32';
elementData(1).dataFormat = '%8.4f';
elementData(1).nComp      = '1';
elementData(1).data       = elementTable(:,1:2);

% material parameter set ID
elementData(2).fieldName  = 'matID';
elementData(2).dataType   = 'Float32';
elementData(2).dataFormat = '%8.4f';
elementData(2).nComp      = '1';
elementData(2).data       = elementTable(:,[1,3]);

% load parameter set ID
elementData(3).fieldName  = 'loadID';
elementData(3).dataType   = 'Float32';
elementData(3).dataFormat = '%8.4f';
elementData(3).nComp      = '1';
elementData(3).data       = elementTable(:,[1,4]);

% ----------------------------------------------------------------------- %


% -- Write csv data to vtk file ----------------------------------------- %

vtk.WriteVtkFile.WriteVtkFile(vtkFileData, coord, connectExt, nodeData, elementData, parallelExecution);

% ----------------------------------------------------------------------- %

end


% ======================================================================= %
% ======================================================================= %
