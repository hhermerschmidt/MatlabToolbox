function [coordMinRange, coordMaxRange] = EvalCoordRangeExt(coord, rangeFac)
% Function evaluating the range of the given coordinates and extends
% these range by the factor rangeFac.
%
% Input
% -----
% coord   : nx1 matrix of n coordinates
% rangeFac: float value
%
% Output
% ------
% coordMinRange: float, min value of extended coordinate range
% coordMaxRange: float, max value of extended coordinate range
%
% Usage
% -----
% [coordMinRange, coordMaxRange] = EvalCoordRangeExt(coord, rangeFac)

% created by Henning Schippke
% created on       14.04.2017
% last modified on 14.04.2017


coordMin      = min(coord);
coordMax      = max(coord);

coordRange    = coordMax - coordMin;

coordMinRange = coordMin - rangeFac * coordRange;
coordMaxRange = coordMax + rangeFac * coordRange;

end