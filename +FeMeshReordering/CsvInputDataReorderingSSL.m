function [ ] = CsvInputDataReorderingSSL(folder, nameCalcExample)

% created by   : Henning Schippke
% created on   : 16.04.17
% last modified: 16.04.17

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Load fe mesh data from csv files ----------------------------------- %

inputFolder  = folder.inputDataFolder;
outputFolder = folder.outputDataFolder;

%import FEM2FlowUtilities.CsvInputDataFiles.LoadCsvData
import FEM2FlowUtilities.CsvInputDataFiles.*

% coord        = LoadCsvData(inputFolder, 'nodeCoordinates.csv' );
% % coord : nNode x 4 matrix
% %         [ nodeNumber | xCoord | yCoord | zCoord ]

connect      = LoadCsvData(inputFolder, 'nodeConnectivity.csv');
% connect : nEle x 4 matrix
%           [ elementNumber | connectivity as 1 x nNodePerEle matrix ]
%

% elementTable = LoadCsvData(inputFolder, 'elementTable.csv'    );
% % elementTable : nEle x 4 matrix
% %                [ elementNumber | elementTypeID | matID | loadID ]

sslConnect      = LoadCsvData(inputFolder, 'meshMovSSL.csv');
% sslconnect : nEleSSL x 4 matrix
%              [ elementNumber | connectivity as 1 x nNodePerEle matrix ]
%

sslIntNodes = LoadCsvData(inputFolder, 'meshMovSSLNodesInt.csv');
% sslIntNodes : nNodesSSLInt x 1 vector

% ----------------------------------------------------------------------- %


  sslConnectReordered                    = RearrangeSSLElementOrderung(sslIntNodes, sslConnect);
[ sslConnectRotated  , connectRotated]   = RotateSSLElements(          sslIntNodes, sslConnectReordered, connect);



% -- Save data to csv files --------------------------------------------- %

BasicUtilities.CreateFolder(outputFolder,[]);

import FEM2FlowUtilities.CsvInputDataFiles.*

% copy unchanged data
copyfile([inputFolder,'nodeCoordinates.csv'   ], [outputFolder,'nodeCoordinates.csv'   ]);
copyfile([inputFolder,'elementTable.csv'      ], [outputFolder,'elementTable.csv'      ]);
copyfile([inputFolder,'condBoundDir.csv'      ], [outputFolder,'condBoundDir.csv'      ]);
copyfile([inputFolder,'condInitial.csv'       ], [outputFolder,'condInitial.csv'       ]);

copyfile([inputFolder,'meshMovNodes.csv'      ], [outputFolder,'meshMovNodes.csv'      ]);
copyfile([inputFolder,'meshMovNodesBound.csv' ], [outputFolder,'meshMovNodesBound.csv' ]);
copyfile([inputFolder,'meshMovSSLNodesInt.csv'], [outputFolder,'meshMovSSLNodesInt.csv']);



% write rearraged (changed) data
WriteConnectivity2Csv(outputFolder, 'nodeConnectivity.csv' , nameCalcExample, connectRotated      );
WriteConnectivity2Csv(outputFolder, 'meshMovSSL.csv'       , nameCalcExample, sslConnectRotated   );

% ----------------------------------------------------------------------- %


end %function



function [ sslConnectSorted ] = RearrangeSSLElementOrderung(sslIntNodes, sslConnect)
% Korrektur Reihenfolge SSL-Elemente

sslConnectSorted = zeros(size(sslConnect));

sslConnectNodes  = sslConnect(:,2:end);

for ii = 1:length(sslIntNodes)

    [row1, ~ ] = find(sslConnectNodes == sslIntNodes(ii));

    if ii == length(sslIntNodes)
        [row2, ~ ] = find(sslConnectNodes == sslIntNodes(1));
    else
        [row2, ~ ] = find(sslConnectNodes == sslIntNodes(ii+1));
    end

    currElementSSL = intersect(row1, row2);

    sslConnectSorted(ii,:) = sslConnect(currElementSSL,:);

end %for

end %function "RearrangeSSLElementOrderung"


function [sslConnectRot, globalConnect] = RotateSSLElements(sslIntNodes, sslConnect, globalConnect)
% Drehen der Elemente

sslConnectRot      = zeros(size(sslConnect));
sslConnectRot(:,1) = sslConnect(:,1);

kk = 1;
for ii = 1:size(sslConnect,1) %loop over all SSL elements

    currEleNumb = sslConnect(ii,1);

    posNode     = zeros(1,4);

    % get local position in SSL element of (first) node from SSL node
    % vector
    posNode(1)  = find(sslConnect(ii,2:end) == sslIntNodes(kk));

    posNode(2)  = posNode(1)+1;
    posNode(3)  = posNode(1)+2;
    posNode(4)  = posNode(1)+3;

    for jj = 2:4

        if posNode(jj) > 4
            posNode(jj) = posNode(jj) - 4;
        end
    end

    currInzi                = sslConnect(ii,2:end);
    sslConnectRot(ii,2:end) = [ currInzi(posNode(1)) , currInzi(posNode(2)) , currInzi(posNode(3)) , currInzi(posNode(4)) ];

    globalConnect(globalConnect(:,1) == currEleNumb,2:end) = sslConnectRot(ii,2:end);

    kk = kk + 1;

end %for


end %function
