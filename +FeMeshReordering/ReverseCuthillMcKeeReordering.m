function [coordNewExt, connectNewExt, lutNodeNumbers] = ReverseCuthillMcKeeReordering(connect, coord)
% Function performing reverse Cuthill-McKee on given node connectivity.
%
%
% Input Data:
% --------------
%
% connect : nEle x 3 matrix
%           [ connectivity as 1 x nNodePerEle matrix ]
%           element numbers are assumed to start with 1 and increase strictly
%           monotonically; hence the row number of <connect> specifies
%           implicitly the element number.
%
% coord : nNode x 3 matrix
%         [ x | y | z ]
%         node numbers are assumed to start with 1 and increase strictly
%         monotonically; hence the row number of <coord> specifies
%         implicitly the node number.
%
%
% Output Data:
% ---------------
% coordNewExt: nNode x 4 matrix
%              [ nodeNumb | x | y | z ]
%
% connectNewExt: nEle x (nNodePerEle + 1) matrix
%                [ eleNumb | nodePerEle ]
%
% lutNodeNumbers: look up table mapping old node numbers to new node
%                 numbers
%                 [old node number | new node number ]
%
%
% Usage:
% ---------
% [coordNewExt, connectNewExt, lutNodeNumbers] = ReverseCuthillMcKeeReordering(connect, coord)

% created by   : Henning Schippke
% created on   : 22.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ======================================================================= %
% ======================================================================= %

display('... perform reverse Cuthill-McKee reordering');

% Get node connectivity matrix
[nodeConnectStruct, ~ ] = FeMeshAnalysis.ComputeNodeConnectivityMatrix(connect);

K = nodeConnectStruct.K;

% Perform reverse Cuthill-McKee
rcmP = symrcm(K); % permutation vector

% new node numbers (in strictly monotonically increasing order)
rcmNodeNumbers = (1:length(rcmP))';

% Create look up table for mapping old node numbers to new node numbers
%                [old node number | new node number ]
lutNodeNumbers = [    rcmP'       ,  rcmNodeNumbers ];


% -- Rearrange nodes ---------------------------------------------------- %

rcmCoord = coord(rcmP,:);

% create new extended (node numbers included) coordinate matrix
newNodeNumbers = ( 1:size(rcmCoord,1) )';
coordNewExt    = [newNodeNumbers, rcmCoord];

% ----------------------------------------------------------------------- %


% -- Rearrange connectivity --------------------------------------------- %

rcmConnect = zeros(size(connect));

for mm = 1:size(connect,1)

    currConnect    = connect(mm,:);
    rcmCurrConnect = zeros(size(currConnect));

    for ii = 1:length(currConnect)

        newNodeNumber = find(rcmP == currConnect(ii) );

        rcmCurrConnect(ii) = newNodeNumber;
    end

    rcmConnect(mm,:) = rcmCurrConnect;

end

% create new extended (element numbers included) connectivity matrix
elementNumbers = ( 1:size(rcmConnect,1) )';
connectNewExt  = [elementNumbers, rcmConnect];

% ----------------------------------------------------------------------- %


end %function

% ======================================================================= %
% ======================================================================= %
