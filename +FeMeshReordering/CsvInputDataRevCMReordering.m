function [coordRcm, connectRcm] = CsvInputDataRevCMReordering(folder, nameCalcExample)
% Function reordering node connectivity (stored in csv data files) using
% reverse Cuthill-McKee. Corresponding node data is adjusted appropriately.
%
% Input Data:
% --------------
% folder: structure
%     .inputDataFolder:  string containing path to input  data folder
%     .outputDataFolder: string containing path to output data folder
%
% nameCalcExample: string stating name of calculation example (stored in
%                  header of *.csv files)
%
% Output Data:
% ---------------
% coordRcm:   node coordinates  after reverse Cuthill-McKee
% connectRcm: node connectivity after reverse Cuthill-McKee
%
% Usage:
% ---------
% [coordRcm, connectRcm] = CsvInputDataRevCMReordering(folder, nameCalcExample)

% created by   : Henning Schippke
% created on   : 23.04.15
% last modified: 20.04.17

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% -- Load fe mesh data from csv files ----------------------------------- %

inputFolder  = folder.inputDataFolder;
outputFolder = folder.outputDataFolder;

import FEM2FlowUtilities.CsvInputDataFiles.LoadCsvData

coord        = LoadCsvData(inputFolder, 'nodeCoordinates.csv' );
% coord : nNode x 4 matrix
%         [ nodeNumber | xCoord | yCoord | zCoord ]

connect      = LoadCsvData(inputFolder, 'nodeConnectivity.csv');
% connect : nEle x 4 matrix
%           [ elementNumber | connectivity as 1 x nNodePerEle matrix ]
%

elementTable = LoadCsvData(inputFolder, 'elementTable.csv'    );
% elementTable : nEle x 4 matrix
%                [ elementNumber | elementTypeID | matID | loadID ]

% ----------------------------------------------------------------------- %


% -- Check node and element numbering ----------------------------------- %

nodeNumb = coord(:,1);
nNode    = length(nodeNumb);
if ~all(nodeNumb' == 1:nNode)
    warning('Warn:NodeNumb',['\nNode numbers are not in strictly monotonously increasing order starting with 1.\n',...
                             'Performing reverse Cuthill-McKee may lead to wrong results.']);
end

elementNumb  = connect(:,1);
nElementNumb = length(elementNumb);
if ~all(elementNumb' == 1:nElementNumb)
    warning('Warn:NodeNumb',['Element numbers are not in strictly monotonously increasing order starting with 1.\n',...
                             'Performing reverse Cuthill-McKee may lead to wrong results.']);
end

% neglect node and element numbers
coord   = coord(:,2:end);
connect = connect(:,2:end);

% ----------------------------------------------------------------------- %


% -- Perform rev. Cuthill-McKee on node connectivity -------------------- %

[coordRcm, connectRcm, lutNodeNumbers] = FeMeshReordering.ReverseCuthillMcKeeReordering(connect, coord);

% ----------------------------------------------------------------------- %


% -- Rearrange node data fields ----------------------------------------- %

condBoundDir    = LoadCsvData(inputFolder, 'condBoundDir.csv' );

if ~isempty(condBoundDir)
    condBoundDirNew = RearrangeNodeData(condBoundDir, lutNodeNumbers);
else
    condBoundDirNew = condBoundDir;
end


condInitial    = LoadCsvData(inputFolder, 'condInitial.csv');

if ~isempty(condInitial)
    condInitialNew = RearrangeNodeData(condInitial, lutNodeNumbers);
else
    condInitialNew = condInitial;
end


meshMovNodes    = LoadCsvData(inputFolder, 'meshMovNodes.csv');

if ~isempty(meshMovNodes)
    meshMovNodesNew = RearrangeNodeData(meshMovNodes, lutNodeNumbers);
else
    meshMovNodesNew = meshMovNodes;
end


meshMovNodesBound    = LoadCsvData(inputFolder, 'meshMovNodesBound.csv');

if ~isempty(meshMovNodesBound)
    meshMovNodesBoundNew = RearrangeNodeData(meshMovNodesBound, lutNodeNumbers);
else
    meshMovNodesBoundNew = meshMovNodesBound;
end


meshMovSSLNodesInt    = LoadCsvData(inputFolder, 'meshMovSSLNodesInt.csv');

if ~isempty(meshMovSSLNodesInt)
    meshMovSSLNodesIntNew = RearrangeNodeData(meshMovSSLNodesInt, lutNodeNumbers);
else
    meshMovSSLNodesIntNew = meshMovSSLNodesInt;
end


meshMovSSL    = LoadCsvData(inputFolder, 'meshMovSSL.csv');

if ~isempty(meshMovSSL)
    meshMovSSLNew = RearrangeConnectivitySSL(meshMovSSL, lutNodeNumbers);
else
    meshMovSSLNew = meshMovSSL;
end

% ----------------------------------------------------------------------- %


% -- Save data to csv files --------------------------------------------- %

BasicUtilities.CreateFolder(outputFolder,[]);

import FEM2FlowUtilities.CsvInputDataFiles.*

% write rearranged node coordinates and connectivity
WriteCoord2Csv(outputFolder            , 'nodeCoordinates.csv'  , nameCalcExample, coordRcm        );
WriteConnectivity2Csv(outputFolder     , 'nodeConnectivity.csv' , nameCalcExample, connectRcm      );

% write element data field (unchanged)
WriteElementTable2Csv(outputFolder     , 'elementTable.csv'     , nameCalcExample, elementTable    );

% write node data fields (rearranged)
if ~isempty(condBoundDirNew)
    WriteNodeDataDirichlet2Csv(outputFolder, 'condBoundDir.csv'     , nameCalcExample, condBoundDirNew );
end

if ~isempty(condInitialNew)
    WriteNodeDataIc2Csv(outputFolder       , 'condInitial.csv'      , nameCalcExample, condInitialNew  );
end

% write (rearranged) data related to mesh movement (if present)
if ~isempty(meshMovNodesNew)
    WriteMeshMovNodeData2Csv(outputFolder, 'meshMovNodes.csv', nameCalcExample, meshMovNodesNew);
end

if ~isempty(meshMovNodesBoundNew)
    WriteMeshMovNodeData2Csv(outputFolder, 'meshMovNodesBound.csv', nameCalcExample, meshMovNodesBoundNew);
end

if ~isempty(meshMovSSLNodesIntNew)
    WriteMeshMovNodeData2Csv(outputFolder, 'meshMovSSLNodesInt.csv', nameCalcExample, meshMovSSLNodesIntNew);
end

if ~isempty(meshMovSSLNew)
    WriteMeshMovConnectData2Csv(outputFolder, 'meshMovSSL.csv', nameCalcExample, meshMovSSLNew);
end

% ----------------------------------------------------------------------- %


end %function

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [nodeDataMatrixNew] = RearrangeNodeData(nodeDataMatrix, lut )
% Function replacing old node numbers by new ones using the look up table
% lut.
%
%
% Input Data:
% --------------
%
% nodeDataMatrix: nNodes x var matrix with node associated data.
%                 The first column contains the (old) node numbers.
%
% lut: nNode x 2 matrix
%      look up table mapping old node numbers to new ones.
%      [ old NodeNumber | newNodeNumber ]
%
%
% Output Data:
% ---------------
%
% nodeDataMatrixNew: size(nodeDataMatrix)
%                    matrix with same content as nodeDataMatrix, but
%                    updated node numbers in first column.
%
%
% Usage:
% ---------
% [ nodeDataMatrixNew ] = RearrangeNodeData( nodeDataMatrix, lut )

% created by   : Henning Schippke
% created on   : 23.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


nodeDataMatrixNew = nodeDataMatrix;

for ii = 1:size(lut,1)

    oldNodeNumber = lut(ii,1);
    newNodeNumber = lut(ii,2);

    affectedLines = find(nodeDataMatrix(:,1) == oldNodeNumber);

    nodeDataMatrixNew(affectedLines,1) = newNodeNumber; %#ok<FNDSB>

end

end

% ======================================================================= %

function [rcmConnect] = RearrangeConnectivitySSL(connect, lutNodeNumbers)

% -- Rearrange connectivity --------------------------------------------- %

rcmConnect = zeros(size(connect));

for mm = 1:size(connect,1)

    currConnect    = connect(mm,2:end);
    rcmCurrConnect = zeros(size(currConnect));

    for ii = 1:length(currConnect)

        oldNodeNumber = currConnect(ii);

        newNodeNumber = lutNodeNumbers(lutNodeNumbers(:,1) == oldNodeNumber, 2);

        rcmCurrConnect(ii) = newNodeNumber;
    end

    rcmConnect(mm,1)     = connect(mm,1);
    rcmConnect(mm,2:end) = rcmCurrConnect;

end

end

% ======================================================================= %
% ======================================================================= %
