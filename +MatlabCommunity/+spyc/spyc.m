function spyc(sA,cmap,pb)
% SPYC Visualize sparsity pattern with color-coded scale.
%
% SPYC(S) plots the color-coded sparsity pattern of the matrix S.
%
% SPYC(S,CMAP) plots the sparsity pattern of the matrix S
%              using colormap CMAP.
%
% SPYC(S,CMAP,PB) allows turning off the display of a colorbar by passing
%                 flag PB=0

%   written by Try Hard
%   $Revision: 0.0.0.2 $  $Date: 2013/08/24 11:11:11 $
%
%   modified by Henning Schippke
%   modified on 15.04.2017

% ======================================================================= %
% ======================================================================= %


if nargin<1 || nargin>3 && ~isempty(cmap)
    error( 'spyc:InvalidNumArg', 'spyc takes one to three inputs')
end

if isempty(sA)
    error( 'spyc:InvalidArg', 'sparse matrix is empty')
end

if nargin>1 && ~isempty(cmap)
    % colorspy does not check whether your colormap is valid!
    if ~isnumeric(cmap)
        cmap=colormap(cmap);        
    end
else
    cmap=flipud(colormap('jet'));
    %cmap=flipud(colormap('autumn'));
end

if nargin<3 || isempty(pb)
    pb=0;
end
    
indx     = find(sA);
[Nx, Ny] = size(sA);
sA       = full(sA(indx));
ns       = length(indx);
[ix, iy] = ind2sub([Nx Ny],indx);
imap     = round((sA-min(sA))*63/(max(sA)-min(sA)))+1;

figure = gcf;
ax     = gca;


scatter(iy,ix,[],imap,'Marker','.','SizeData',200)

colormap(cmap)
colormap(flipud(colormap))

set(ax,'ydir','reverse')

box on

axis equal;
axis([0 Nx 0 Ny])

xlabel(['nz = ' num2str(ns)]);

if pb
    colorbar
end

end %function spyc



