function [ surfaceNormal ] = ComputeSurfaceNormalQuad4( coord, connect )
% Function computing the surface normal vector of 2D 4-node (finite element)
% quadrilaterals.
%
% Input Data:
% coord:       n x 2 matrix with n coordinates (x,y)
% connect:     m x 1 cell matrix with connectivity data for m elements
%              with 4 nodes per element
%
% Output Data:
% surfaceNormal:  m x 4 matrix with surface normal data for m elements;
%                 column  1  : element number
%                 column 2:4 : (x,y,z) component of surface normal
%
% [surfaceNormal] = ComputeSurfaceNormalQuad4(coord, connect )

% created by Henning Schippke, 20.03.15
% last modification            20.03.15


if ~iscell(connect)
    connect = mat2cell(connect);
end

nEle = size(connect,1);

surfaceNormal = zeros(nEle,4);

for mm = 1:nEle
    
    currConnect = connect{mm};
    currNodes   = coord(currConnect,:);
    
    xA = currNodes(1,:)';
    xB = currNodes(2,:)';
    xD = currNodes(4,:)';
    
    
    xAB = xB - xA;
    xAD = xD - xA;

    n = cross(xAB,xAD);

    nNorm = n / norm(n,2);
    
    surfaceNormal(mm,1)   = mm;
    surfaceNormal(mm,2:4) = nNorm;
    
end %for

end %function