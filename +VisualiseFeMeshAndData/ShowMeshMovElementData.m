function [ varargout ] = ShowMeshMovElementData( visCase        , ...
                                          nodes          , ...
                                          inziConnect    , ...
                                          movData        , ...
                                          movDataStruct)
% Function showing a 2D (finite element) mesh with colored nodes.
% Only those nodes are plotted which occur in movData.
% The coloring is related to ...
% the (initial or boundary) value specified at this node. For each dofID a
% separate figure is created.
%
% Input Data:
% nodes                : n x 2 matrix with n coordinates (x,y)
% inziConnect          : m x 1 cell with connectivity data for m elements
% nodeDataList         : k x 3 matrix with nodal data
%                        nodeDataList(1,:) : node number
%                        nodeDataList(2,:) : dofID
%                        nodeDataList(3,:) : value
% movDataStruct       : structure containing control data for labelling and
%                        plotting (optional)
%     .label.title     : figure name and title of plot
%     .label.x         : x-axis label
%     .label.y         : y-axis label
%     .label.cbar      : colorbar label
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .saveName        : file name for saving
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout     : 1 x 4 cell with handle objects related to the figure
% varargout{1}: figure   handle object
% varargout{2}: axes     handle object
% varargout{3}: patch    handle object (lines of fe mesh)
% varargout{4}: colorbar handle object
%
% [ varargout ] = ShowMeshMovData( nodes, inziConnect, nodeDataList, movDataStruct )

% created by Henning Schippke, 13.04.17
% last modification            13.04.17

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*


% -- Read out optional input values ------------------------------------- %

% show or hide figure
if isfield(movDataStruct,'flagFigVisible')
    flagFigVisible = movDataStruct.flagFigVisible;
else
    flagFigVisible = 'on';
end

% figure position on screen
if isfield(movDataStruct,'figPos')
    figPos = movDataStruct.figPos;
else
    figPos = [];
end

% flag for saving figures as *.pdf and *.fig
if isfield(movDataStruct,'flagSaveFigures')
    flagSaveFigures = movDataStruct.flagSaveFigures;
else
    flagSaveFigures = 0;
end

% create also *.fig file
if isfield(movDataStruct,'flagCreateFigFiles')
    flagCreateFigFiles = movDataStruct.flagCreateFigFiles;
else
    flagCreateFigFiles = 0;
end

% ----------------------------------------------------------------------- %

% create figure
fig = figure('visible',flagFigVisible,'Name',movDataStruct.label.title,'NumberTitle','off');
if ~isempty(figPos)
    set(fig,'Position',figPos);
end

ax = axes(); hold(ax,'on');    %#ok<LAXES>


switch visCase
        
     case 'SSLElements'
        
        inziConnectSSL = movData;
        
        % compute surface normal per element
        import FeMeshAnalysis.ComputeSurfaceNormalQuad4
        [ surfNorm ] = ComputeSurfaceNormalQuad4(nodes, inziConnectSSL);
        
        % show fe mesh with colored nodes related to node data distribution
        [~, ptch, cbar] = PlotFiniteElements( ax, nodes, inziConnectSSL, surfNorm(:,4));
    
        set(get(cbar,'ylabel'),'string','z-direction surface normal')
        
    case 'SSLEleOrdering'
        
        inziConnectSSL = movData;
        
        nEleSSL = size(inziConnectSSL,1);
        eleSSLOrdering = linspace(1,nEleSSL,nEleSSL)';
        
        
        % show fe mesh with colored nodes related to node data distribution
        [~, ptch, cbar] = PlotFiniteElements( ax, nodes, inziConnectSSL, eleSSLOrdering);
        
        inziConnectSSLMat = cell2mat(inziConnectSSL);
        nodeA = inziConnectSSLMat(:,1);

        plot(nodes(nodeA,1), nodes(nodeA,2), 'o','MarkerFaceColor', [0.2, 0.2, 0.2], 'MarkerEdgeColor', [0.2, 0.2, 0.2], 'MarkerSize', 10)
        
        set(get(cbar,'ylabel'),'string','local element ordering')
        
    otherwise
        error('Only "SSLElements" and "SSLEleOrdering" are defined as case for function "ShowMeshMovElementData()."')

end %switch


% reset x and y axis range
nodesRed = nodes([inziConnectSSL{:}],:);

% calculate (extended) axis range
import BasicUtilities.EvalCoordRangeExt
[xMinRange, xMaxRange] = EvalCoordRangeExt(nodesRed(:,1), 0.1);  
[yMinRange, yMaxRange] = EvalCoordRangeExt(nodesRed(:,2), 0.1);  
axis([xMinRange, xMaxRange, yMinRange, yMaxRange]);
hold(ax,'off');

% label
title( ax, movDataStruct.label.title); 
xlabel(ax, movDataStruct.label.x); 
ylabel(ax, movDataStruct.label.y);
    
        
% save figures to file (if wanted)
if flagSaveFigures == 1
    savePath    = movDataStruct.savePath;
    saveNameFig = movDataStruct.saveName;

    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])    

    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end

end %if


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = ptch;

try
    varargout{4} = cbar;
catch
    
end %try

end %function ShowMeshMovElementData

% ======================================================================= %
% ======================================================================= %