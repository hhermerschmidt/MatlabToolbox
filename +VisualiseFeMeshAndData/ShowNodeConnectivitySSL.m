function  [ varargout ] = ShowNodeConnectivitySSL(coord, connect, connectSSL, controlStruct)
% Function showing the node connectivity of a (finite element) mesh via its
% connectivity matrix.
%
% Input Data:
% coord:         n x 2 matrix with n coordinates (x,y)
% connect:       m x 1 cell with connectivity data for m elements
%                with variable number of nodes per element
% connectSSL:    k x 1 cell with connectivity data for k SSL elements
%                with variable number of nodes per SSL element
% controlStruct: controlStructure used for plotting (optional)
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout{1}: figure  handle object
% varargout{2}: axes    handle object
% varargout{3}: spy     handle object (blue dots in spy-plot)
%
% [ varargout ] = ShowNodeConnectivitySSL(coord, connect, controlStruct)

% created  by Henning Schippke, based on ShowNodeConnectivity()
% created  on 15.04.17
% modified on 15.04.17


% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*

% check, if control structure exists and if yes
% read out input values or set default values
if ~isempty(controlStruct)
    
    % show or hide figure
    if isfield(controlStruct,'flagFigVisible')
        flagFigVisible = controlStruct.flagFigVisible;
    else
        flagFigVisible = 'on';
    end
    
    % figure position on screen
    if isfield(controlStruct,'figPos')
        figPos = controlStruct.figPos;
    else
        figPos = [];
    end
    
    % flag for saving figures as *.pdf and *.fig
    if isfield(controlStruct,'flagSaveFigures')
        flagSaveFigures = controlStruct.flagSaveFigures;
    else
        flagSaveFigures = 0;
    end
 
    % set figure save name
    if isfield(controlStruct,'saveName')
        saveNameFig = controlStruct.saveName;
    else
        saveNameFig = 'nodeConnectivity';
    end

    % create also *.fig file
    if isfield(controlStruct,'flagCreateFigFiles')
        flagCreateFigFiles = controlStruct.flagCreateFigFiles;
    else
        flagCreateFigFiles = 0;
    end
      
end

numbNodes = size(coord,1);

% convert connectivity from cell to matrix (if necessary)
if iscell(connect)
    connect = FEM2FlowUtilities.Connectivity.ConvertConnectivity_cell2mat(connect);
end

% compute connectivity matrix
[nodeConnectStruct, bandwidth ] = FeMeshAnalysis.ComputeNodeConnectivityMatrix(connect);

K         = nodeConnectStruct.K;
K_nz      = nodeConnectStruct.K_nz;
K_dim     = nodeConnectStruct.K_dim;
K_density = nodeConnectStruct.K_density;

% set all connectivity values to 1
nz    = find(K);
K(nz) = 1;


% manipulate connectivity matrix making SSL nodes visible
connectSSLMat = cell2mat(connectSSL);
nodesSSL      = reshape(connectSSLMat,[numel(connectSSLMat),1]);
nodesSSL      = unique(nodesSSL);

for nn = 1:length(nodesSSL)
   
    nodeNumber    = nodesSSL(nn);   
    adjacentNodes = find(K(nodeNumber,:) ~= 0);
    
    K(nodeNumber   , adjacentNodes) = 2;
    K(adjacentNodes, nodeNumber) = 2;

end    


% plot bandwith
fig = figure('visible',flagFigVisible,'Name','connectivity matrix with focus on shear slip layer nodes with neighbourhood','NumberTitle','off');
if ~isempty(figPos)
    set(fig,'position', figPos)
end

ax = axes();

% show node connectivity matrix
spy(K); hold on;
spyHandle = findall(ax,'color','b');
spy(K==1,'b');
spy(K==2,'r');
hold off;

% label
title(ax,{'connectivity matrix','shear slip layer nodes with neighbourhood'});
xlabel(ax                                                         , ...
       {['bandwidth: ',                                             ...
         num2str(bandwidth.value), ' / ', num2str(numbNodes),' = ', ...
         num2str(bandwidth.relative),' %'                           ...
        ]                                                           ...
        ['degree of density: ',                                     ...
         num2str(K_nz),' / ',num2str(K_dim),'\^2', '  = ',          ...
         num2str(K_density), ' %'                                   ...
        ]                                                           ...
      });

  
% save figures to file (if wanted)
if flagSaveFigures == 1
    
    savePath    = controlStruct.savePath;
    
    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])
    
    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end
    
end


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = spyHandle;

end %function ShowNodeConnectivity

% ======================================================================= %
% ======================================================================= %