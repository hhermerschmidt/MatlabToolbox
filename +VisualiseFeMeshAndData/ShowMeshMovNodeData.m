function [ varargout ] = ShowMeshMovNodeData( visCase        , ...
                                              nodes          , ...
                                              inziConnect    , ...
                                              movData        , ...
                                              movDataStruct)
% Function showing a 2D (finite element) mesh with colored nodes.
% Only those nodes are plotted which occur in movData.
%
% Input Data
% ----------
% nodes                : n x 2 matrix with n coordinates (x,y)
% inziConnect          : m x 1 cell with connectivity data for m elements
% movData              : k x 1 matrix containing node numbers
% movDataStruct        : structure containing control data for labelling and
%                        plotting (optional)
%     .label.title     : figure name and title of plot
%     .label.x         : x-axis label
%     .label.y         : y-axis label
%     .label.cbar      : colorbar label
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .saveName        : file name for saving
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data
% -----------
% varargout     : 1 x 4 cell with handle objects related to the figure
% varargout{1}: figure   handle object
% varargout{2}: axes     handle object
% varargout{3}: patch    handle object (lines of fe mesh)
% varargout{4}: colorbar handle object [! only if available !]
%
% Usage
% -----
% [ varargout ] = ShowMeshMovNodeData( nodes, inziConnect, nodeDataList, movDataStruct )

% created by Henning Schippke, 13.04.17
% last modification            20.04.17

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*


% -- Read out optional input values ------------------------------------- %

% show or hide figure
if isfield(movDataStruct,'flagFigVisible')
    flagFigVisible = movDataStruct.flagFigVisible;
else
    flagFigVisible = 'on';
end

% figure position on screen
if isfield(movDataStruct,'figPos')
    figPos = movDataStruct.figPos;
else
    figPos = [];
end

% flag for saving figures as *.pdf and *.fig
if isfield(movDataStruct,'flagSaveFigures')
    flagSaveFigures = movDataStruct.flagSaveFigures;
else
    flagSaveFigures = 0;
end

% create also *.fig file
if isfield(movDataStruct,'flagCreateFigFiles')
    flagCreateFigFiles = movDataStruct.flagCreateFigFiles;
else
    flagCreateFigFiles = 0;
end

% ----------------------------------------------------------------------- %


% create figure
fig = figure('visible',flagFigVisible,'Name',movDataStruct.label.title,'NumberTitle','off');
if ~isempty(figPos)
    set(fig,'Position',figPos);
end

ax = axes(); hold(ax,'on');    %#ok<LAXES>


switch visCase

    case 'SSLNodesInt' % visualise interior nodes of SSL

        nNodesSSLInt = length(movData);
        colorData    = linspace(1, nNodesSSLInt, nNodesSSLInt);

        % show fe mesh with colored nodes related to node data distribution
        [~, ptch   ] = PlotFiniteElements(ax, nodes, inziConnect);
        [~, ~, cbar] = PlotNodes(ax, [nodes(movData,1), nodes(movData,2)], colorData );

        set(get(cbar,'ylabel'),'string','local node ordering')


    case 'MovNodes' % visualise all movable nodes

        % show fe mesh with colored nodes
        markerSize  = 20;
        markerColor = 'g';
        markerStyle = 'filled';

        [~, ptch   ] = PlotFiniteElements(ax, nodes, inziConnect);
        scat         = scatter(nodes(movData,1),nodes(movData,2),markerSize,markerColor,markerStyle);

    case 'MovNodesBound' % visualise all movable boundary nodes

        % show fe mesh with colored nodes
        markerSize  = 20;
        markerColor = 'g';
        markerStyle = 'filled';

        [~, ptch   ] = PlotFiniteElements(ax, nodes, inziConnect);
        scat         = scatter(nodes(movData,1),nodes(movData,2),markerSize,markerColor,markerStyle);

    otherwise
        error('Only "SSLNodesInt", "MovNodes" and "MovNodesBound" are defined as cases for function "ShowMeshMovNodeData()."')

end %switch


% reset x and y axis range
import BasicUtilities.EvalCoordRangeExt
[xMinRange, xMaxRange] = EvalCoordRangeExt(nodes(movData,1), 0.2);
[yMinRange, yMaxRange] = EvalCoordRangeExt(nodes(movData,2), 0.2);

axis([xMinRange, xMaxRange, yMinRange, yMaxRange]);
hold(ax,'off');


% label
title( ax, movDataStruct.label.title);
xlabel(ax, movDataStruct.label.x);
ylabel(ax, movDataStruct.label.y);


% save figures to file (if wanted)
if flagSaveFigures == 1
    savePath    = movDataStruct.savePath;
    saveNameFig = movDataStruct.saveName;

    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])

    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end

end %if


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = ptch;

try
    varargout{4} = cbar;
catch

end %try

end %function ShowMeshMovNodeData

% ======================================================================= %
% ======================================================================= %
