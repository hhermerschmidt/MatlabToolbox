function [ varargout ] = ShowFiniteElementMesh( nodes, inziConnect, controlStruct )
% Function showing a 2D (finite element) mesh with element edges as lines
% and nodes as dots. The element faces are colored using the value of the
% z-direction of the (normalised) surface normal.
%
% Input Data:
% nodes:         n x 2 matrix with n coordinates (x,y)
% inziConnect:   m x 1 cell with connectivity data for m elements
%                with variable number of nodes per element
% controlStruct: controlStructure used for plotting (optional)
%     .figPos          : screen position of figure (optional)
%     .flagSaveFigures : flag stating if the screen figure is saved to file
%                        0 = no (default), 1 = yes
%     .savePath        : path to folder for figure to be saved in
%                        (has to be specified only, if flagSaveFigures = 1)
%     .flagFigVisible  : flag specifying if figure is displayed ('on') or not
%                        ('off')
%     .flagCreateFigFiles: 0 : no *.fig files are created (pdf only)
%                          1 : *.fig and *.pdf files are created
%
% Output Data:
% varargout{1}: figure  handle object
% varargout{2}: axes    handle object
% varargout{3}: patch   handle object (lines of fe mesh)
% varargout{4}: scatter handle object (nodes of fe mesh)
% varargout{5}: colobar handle object
%
% [ varargout ] = ShowFiniteElementMesh( nodes, inziConnect, controlStruct )

% created by Henning Schippke, 20.03.15
% last modification            22.08.15


% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package functions
import VisualiseFeMeshAndData.*

% check, if control structure exists and if yes
% read out input values or set default values
if ~isempty(controlStruct)
    
    % show or hide figure
    if isfield(controlStruct,'flagFigVisible')
        flagFigVisible = controlStruct.flagFigVisible;
    else
        flagFigVisible = 'on';
    end
    
    % figure position on screen
    if isfield(controlStruct,'figPos')
        figPos = controlStruct.figPos;
    else
        figPos = [];
    end
    
    % flag for saving figures as *.pdf and *.fig
    if isfield(controlStruct,'flagSaveFigures')
        flagSaveFigures = controlStruct.flagSaveFigures;
    else
        flagSaveFigures = 0;
    end
    
    % create also *.fig file
    if isfield(controlStruct,'flagCreateFigFiles')
        flagCreateFigFiles = controlStruct.flagCreateFigFiles;
    else
        flagCreateFigFiles = 0;
    end

end

numbNodes = size(nodes,1);
numbEle   = size(inziConnect,1);

% compute surface normal per element
import FeMeshAnalysis.ComputeSurfaceNormalQuad4
[ surfNorm ] = ComputeSurfaceNormalQuad4(nodes, inziConnect);

% plot finite element mesh
fig = figure('visible',flagFigVisible,'Name','finite element mesh','NumberTitle','off');
if ~isempty(figPos)
    set(fig,'Position',figPos);
end

ax = axes();
hold(ax,'on');

% show finite element mesh with nodes and colored
% using z-direction of surface normal
[~, ptch, cbar] = PlotFiniteElements(ax, nodes, inziConnect, surfNorm(:,4));
[~, scat      ] = PlotNodes(ax, nodes);

hold(ax,'off');

% label
title(ax                              , ...
      {['finite element mesh']        , ...
       ['('                           , ...
        num2str(numbNodes),' nodes, ' , ...
        num2str(numbEle)  ,' elements', ...
        ')'                             ...
       ]                                ...
     }); %#ok<NBRAK>
 
xlabel(ax,'x [m]');  
ylabel(ax,'y [m]'); 
   
set(get(cbar,'ylabel'),'string','z-direction surface normal')


% save figures to file (if wanted)
if flagSaveFigures == 1
    
    savePath    = controlStruct.savePath;
    saveNameFig = 'spatialDiscretisation';
    
    saveas(fig,[savePath,'pdf/',saveNameFig,'.pdf'])
    if flagCreateFigFiles == 1
        saveas(fig,[savePath,'fig/',saveNameFig,'.fig'])
    end

end


% return values
varargout{1} = fig;
varargout{2} = ax;
varargout{3} = ptch;
varargout{4} = scat;
varargout{5} = cbar;

end %function ShowFiniteElementMesh

% ======================================================================= %
% ======================================================================= %