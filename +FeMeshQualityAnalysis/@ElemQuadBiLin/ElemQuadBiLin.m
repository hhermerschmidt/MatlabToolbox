classdef ElemQuadBiLin < handle
% Class creating and an object of a 4 node quadrilateral finite element for
% which the Jacobian matrix and its determinant can be calculated at the
% nodal points.
%
% The node coordinates as well as the position of the first node have to be 
% specified ('LowLeft', 'LowRight', 'UpRight', 'UpLeft').
%
%
% Properties:
% --------------
%
% nodeCoordLoc : 4 x 2 matrix
%                node coordinates in normalised reference system (xi, eta)
%
% nodeCoordGlob: 4 x 2 matrix
%                node coordinates in real physical system (x,y)
%
% nNodePerEle  : int
%                number of nodes per element
%
% matJNode     : 2 x 1 cell array
%                each cell contains a 2x2 matrix containing the Jacobian matrix
%                of each node
%
% detJNode     : 4 x 1 matrix
%                determinant of the Jacobian matrix at each node
%
%
% Methods:
% -----------
% 
% EvalJacobian(): Function evaluating the Jacobian matrix and its
%                 determinant at every node.
%
%
% Usage:
% ---------
%
% coord = [0, 0, 0; ...
%          5, 0, 0; ...
%          5, 2, 0; ...
%          0, 2, 0 ];
% 
% posStartNode = 'LowLeft';
%
% testElem = FeMeshQualityAnalysis.ElemQuadBiLin(coord, posStartNode);
% testElem.EvalJacobian

% created  by Henning Schippke
% created  on 01.06.2015
% modified on 06.02.2016

% TO DO:
% - ...

% ======================================================================= %
% ======================================================================= %

properties
    
    nodeCoordLoc  % node coordinates in normalised reference system (xi, eta)
    nodeCoordGlob % node coordinates in real physical system (x,y)
    
    nNodePerEle
   
    matJNode
    detJNode

end

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% methods
% ======================================================================= %

methods


function elem = ElemQuadBiLin(coordInput, posStartNode)

    elem.SetNodeCoordGlob(coordInput);
    elem.SetNodeCoordLoc(posStartNode);
    
    elem.SetNodePerEle;
    
    nNode = elem.GetNodePerEle;
    elem.matJNode =  cell(nNode,1);
    elem.detJNode = zeros(nNode,1);
    

end %constructor

% ======================================================================= %
% ======================================================================= %


function EvalJacobian(elem)

    
    coordGlob = elem.GetNodeCoordGlob;
    coordLoc  = elem.GetNodeCoordLoc;
    
    nNode     = elem.GetNodePerEle;
    
    for nn = 1:nNode
       
        evalCoord = coordLoc(nn,:);
        
        [~, Ndxi, Ndeta] = elem.EvalFunc(coordLoc, evalCoord);
        
        NdXI = [ Ndxi, Ndeta ];
        
        J = coordGlob' * NdXI;
        detJ = det(J);
        
        elem.SetJacobiMatNode(nn, J);
        elem.SetJacobiDetNode(nn, detJ);
   
    end
    
end


% ======================================================================= %

function SetJacobiMatNode(elem, nodeNumb, jacobiMat)
   
    elem.matJNode{nodeNumb} = jacobiMat;    
    
end

% ======================================================================= %

function matJ = GetJacobiMatNode(elem, nodeNumb)
   
    matJ = elem.matJNode{nodeNumb};
    
end


% ======================================================================= %

function matJ = GetJacobiMat(elem)
    
    matJ = elem.matJNode;
    
end

% ======================================================================= %


function SetJacobiDetNode(elem, nodeNumb, jacobiDet)
   
    elem.detJNode(nodeNumb) = jacobiDet;
    
end

% ======================================================================= %


function detJ = GetJacobiDetNode(elem, nodeNumb)
   
    detJ = elem.detJNode(nodeNumb);
    
end

% ======================================================================= %

function detJ = GetJacobiDet(elem)
    
    detJ = elem.detJNode;

end

% ======================================================================= %


function SetNodePerEle(elem)
   
    coordLoc           = elem.GetNodeCoordLoc;
    numbNodePerElement = size(coordLoc,1);
    
    elem.nNodePerEle   = numbNodePerElement;
    
end

% ======================================================================= %


function numb = GetNodePerEle(elem)
   
    numb = elem.nNodePerEle;
    
end

% ======================================================================= %


function SetNodeCoordGlob(elem, coordInput)
   
    elem.nodeCoordGlob = coordInput(:,1:2);
    
end

% ======================================================================= %


function nodeCoordGlob = GetNodeCoordGlob(elem)
    
    nodeCoordGlob = elem.nodeCoordGlob;
end

% ======================================================================= %


function SetNodeCoordLoc(elem, posStartNode)
% Function returning the local coordinates (xi, eta) of the four bilinear
% ansatz functions applied on or in order to describe a quadrilateral
% domain and functional variation on it respectively.

    switch posStartNode
        case 'LowLeft'
            % node coordinates
            %               xi, eta
            coord = [ -1, -1 ; ...
                       1, -1 ; ...
                       1,  1 ; ...
                      -1,  1 ; ...
                    ];

        case 'LowRight'
            % node coordinates
            %               xi, eta
            coord = [  1, -1 ; ...
                       1,  1 ; ...
                      -1,  1 ; ...
                      -1, -1 ; ...
                    ];

        case 'UpRight'
            % node coordinates
            %               xi, eta
            coord = [  1,  1 ; ...
                      -1,  1 ; ...
                      -1, -1 ; ...
                       1, -1 ; ...
                    ];

        case 'UpLeft'
            % node coordinates
            %               xi, eta
            coord = [ -1,  1 ; ...
                      -1, -1 ; ...
                       1, -1 ; ...
                       1,  1 ; ...
                    ];

        otherwise
            display('This starting for node numbering is not defined.');

    end %switch

    elem.nodeCoordLoc = coord;        

end %function 

% ======================================================================= %


function nodeCoordLoc = GetNodeCoordLoc(elem)

    nodeCoordLoc = elem.nodeCoordLoc;

end %function GetNodeCoordLoc

% ======================================================================= %


end %methods

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% static methods
% ======================================================================= %

methods(Static)

function [N, Ndxi, Ndeta] = EvalFunc(nodeCoordLoc, evalCoordLoc)
% Function evaluating the bilinear ansatz functions on the square domain
% (xi, eta) = [-1,1] x [-1, 1] at point given by 'evalCoordLoc'. The nx2
% matrix 'nodeCoordLoc' specifies the local node coordinates of the n
% ansatz function N_i, i = 1,2,3,4, to be evaluated.
%
% Input Data:
% nodeCoordLoc: nx2 matrix with (xi,eta) for n nodes.
% evalCoordLoc: 1x2 vector with (xi, eta) cordinates for evaluation point.
%
% Output Data:
% N     : nx1 vector with n evaluated ansatz functions 
%         at evalCoordLoc (xi, eta)
% Ndxi  : nx1 vector with n evaluated derived by xi ansatz functions
%         at evalCoordLoc (xi, eta)
% Ndeta : nx1 vector with n evaluated derived by eta ansatz functions
%         at evalCoordLoc (xi, eta)

N     = 1/4                                          ...
         * (1 + nodeCoordLoc(:,1) * evalCoordLoc(1)) ...
        .* (1 + nodeCoordLoc(:,2) * evalCoordLoc(2));

Ndxi  = 1/4 * nodeCoordLoc(:,1) .* (1 + nodeCoordLoc(:,2) * evalCoordLoc(2));
Ndeta = 1/4 * nodeCoordLoc(:,2) .* (1 + nodeCoordLoc(:,1) * evalCoordLoc(1));

end %function EvalFunc

% ======================================================================= %


end %methods static

% ======================================================================= %
% ======================================================================= %


end %classdef

% ======================================================================= %
% ======================================================================= %