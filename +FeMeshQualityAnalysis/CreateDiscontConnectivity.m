function [coordDisct, connectDisct, lutNodeNumber] = CreateDiscontConnectivity(coord, connect)
% Function generating a discontinuous connectivity from a given continuous
% one.
%
% Input Data:
% --------------
%
% coord:   node coordinates
%          nNode x 4 matrix
%          [ nodeNumber | xCoord | yCoord | zCoord ]
%
% connect: element connectivity
%          nEle x (nNodesPerEle + 1) matrix
%          [ elementNumber | nodeA | nodeB | ... | nodeZ ]

nEle           = size(connect,1);
nNodesPerEle   = size(connect,2)-1;
coordDisctNumb = nEle * nNodesPerEle; % nNodes for discont connectivity 

nNode          = size(coord,1);
dimension      = size(coord,2)-1; % spatial dimension

coordDisct     = zeros(coordDisctNumb,dimension+1);
connectDisct   = zeros(size(connect));


lutNodeNumber = zeros(coordDisctNumb,3); % look-up table mapping
                                         % old node numbers to new one

cc = 1; % counter = newNodeNumber

for jj = 1:nEle %loop over all elements

    currEleNumb = connect(jj,1);
    currNodes   = connect(jj,2:end);
        
    % elementwise/discontinuous node numbering
    ccBegin        = cc;
    ccEnd          = cc + length(currNodes)-1;  
    counterVector  = ccBegin:ccEnd;
    
    newNodeNumbers = counterVector;

    % create look-up table
    lutNodeNumber(counterVector,1) = currEleNumb;
    lutNodeNumber(counterVector,2) = currNodes;
    lutNodeNumber(counterVector,3) = newNodeNumbers;
    
    % create connectivity matrix for discont connectivity  
    newNodeNumberVector = zeros(size(currNodes));
    
    for ii = 1:length(currNodes)
        oldNodeNumber    = currNodes(ii);
        coordinateVector = coord( coord(:,1) == oldNodeNumber, 2:end);
        
        lutRed        = lutNodeNumber( lutNodeNumber(:,1) == currEleNumb  , : );           
        newNodeNumber = lutRed(        lutRed(:,2)        == oldNodeNumber, 3 );
        
        newNodeNumberVector(ii)          = newNodeNumber;                
        coordDisct(counterVector(ii),:)  = [newNodeNumber, coordinateVector];
    end
    
    connectDisct(jj,:) = [currEleNumb, newNodeNumberVector];
    
    cc = cc + length(currNodes);
end

end