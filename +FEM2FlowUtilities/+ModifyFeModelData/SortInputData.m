function [] = SortInputData( sortingCriteria, nameCalcExample, path2CallingScript )
% Function sorting data from FE model csv input data files wrt ascending
% element or node numbers.
%
%
% InputData:
% ----------
%
% sortingCriteria: string specifying wrt which criteria sorting will be performed
%   'elementBased': sorting data wrt element number in ascending order
%   'nodeBased'   : sorting data wrt node    number in ascending order
%
% nameCalcExample : string with name of current calculation exmaple
%
% path2CallingScript: string with (absolute) path to script calling this function
%                     (no trailing '/')
%
%
% OutputData:
% -----------
%
% []: None
%
%
% Usage:
% ------
%
% [] = SortInputData( sortingCriteria, nameCalcExample, path2CallingScript )


% created by   : Henning Schippke
% created on   : 24.01.17
% last modified: 25.01.17

% ================================================================= %
% ================================================================= %


switch sortingCriteria

    case 'elementBased'

        SortElementTable(     nameCalcExample, path2CallingScript);
        SortNodeConnectivity( nameCalcExample, path2CallingScript);


    case 'nodeBased'

        disp('!!! Node based sorting not yet implemented !!!');

    otherwise

        disp('!!! Sorting criteria not defined !!!');

end

end %function

% ================================================================= %
% ================================================================= %


% ================================================================= %
% Subfunctions
% ================================================================= %

function [] = SortElementTable(nameCalcExample, path2CallingScript)
% Function sorting csv-file 'elementTable.csv' in ascending element number
% (i.e. wrt first column of file)

    disp('... sorting element table     by ascending element number');

    % Read in elementTable
    elementTable = importdata([path2CallingScript,'/FeModelData/','elementTable.csv'],',',7);
    elementTable = elementTable.data;

    % Sort elementTable ascending according to first row (= element number)
    elementTableNew = sortrows(elementTable);

    % Save sorted elementTable
    FEM2FlowUtilities.CsvInputDataFiles.WriteElementTable2Csv([path2CallingScript,'/FeModelDataModified/'], 'elementTable.csv', nameCalcExample, elementTableNew)

end


function [] = SortNodeConnectivity(nameCalcExample, path2CallingScript)
% Function sorting csv-file 'nodeConnectivity.csv' in ascending element number
% (i.e. wrt first column of file)

        disp('... sorting node connectivity by ascending element number')

        nodeConnectivity = importdata([path2CallingScript,'/FeModelData/','nodeConnectivity.csv'],',',7);
        nodeConnectivity = nodeConnectivity.data;

        nodeConnectivityNew = sortrows(nodeConnectivity);

        FEM2FlowUtilities.CsvInputDataFiles.WriteConnectivity2Csv([path2CallingScript,'/FeModelDataModified/'], 'nodeConnectivity.csv', nameCalcExample, nodeConnectivityNew)

end

% ================================================================= %
% ================================================================= %
