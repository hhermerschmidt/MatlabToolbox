function [ connectMat ] = ConvertConnectivity_cell2mat( connect )
% Function converting the node connectivity of a (finite element) mesh from
% cell type to matrix type.
%
% In case the number of nodes per element vary, the resulting matrix is
% created with as many columns as the maximum number of nodes per element.
% The connectivity of elements with fewer nodes is filled with NaNs.
%
% Input Data:
% connect:     m x 1 cell with connectivity data for m elements
%              with variable number of nodes per element
%
% Output Data:
% connectMat:  m x nMax matrix with connectivity data for m elements
%              with nMax as maximum number of nodes per element
%
% [connectMat] = ConvertConnectivity_cell2mat(connect)

% created by Henning Schippke, 20.03.15
% last modification:           20.03.15


nEle = size(connect,1);

if iscell(connect)

    % evaluate max number of nodes per element
    maxNumbNodesEle = 0;
    
    for ii = 1:nEle
       
        currInzi     = connect{ii,1};
        numbNodesEle = length(currInzi);

        if numbNodesEle > maxNumbNodesEle
            maxNumbNodesEle = numbNodesEle;
        end %if  
        
    end %for
    
    connectMat = cell(nEle,1);
    
    % expand elements with less nodes by NaNs
    for ii = 1:nEle
       
        currInzi     = connect{ii};
        numbNodesEle = length(currInzi);
        
        if numbNodesEle < maxNumbNodesEle
            
            missingNumbNodes = maxNumbNodesEle - numbNodesEle;
            
            currInzi         = [currInzi, NaN * ones(1,missingNumbNodes)]; %#ok<AGROW>
        end %if        
        
        connectMat{ii} = currInzi;
            
    end %for
    
    % convert cell to matrix
    connectMat = cell2mat(connectMat);
    
end %if

end %function ConvertConnectivity_cell2mat