function [] = VisualiseCsvInputData(folder, flag)
% Function visualising information of fe input data files (*.csv)
% created/extracted from cubit data file (*.cub).
%
% Function reading node coordinates (from nodeCoordinates.csv) and node
% connectivity (from nodeConnectivity.csv) and plotting the fe mesh with
% nodes which are colored via th norm of their element surface normals. This
% picture is saved as spatialDiscretisation.pdf. In addition a node
% connectivity matrix is generated and saved as nodeConnectivity.pdf.
%
% Furthermore element data (such as element number, material and load id) are
% plotted as well as nodal data (such as boundary and initial conditions).
%
% Input Data:
% --------------
% folder: structure
%     .inputDataFolder : string with path to input  data folder
%     .outputDataFoder : string with path to output data folder
%
% flag: structure
%     .saveFigures: 0 :    no pictures are stored; only screen display
%                   1 :    pictures are stored in outputDataFolder and
%                          displayed on screen
%     .figVisible   'off': figures are not displayed during creation
%                   'on' : figures are     displayed during creation
%     .createFigFiles: 0 : no *.fig files are created (pdf only)
%                      1 : *.fig and *.pdf files are created
%
% Output Data:
% ---------------
% [] : none
%
% Usage:
% ---------
% [] = VisualiseCsvInputData( folder, flag )
%

% created by   : Henning Schippke
% created on   : 23.04.15
% last modified: 20.04.17

% ToDo:
% Darstellung Verteilung Knotennummern (bei Fe-Netz oder Konnektiviaet)
% Darstellung Verteilung Elementnummern (bei Fe-Netz)
% Auslessen Ueberschriften/Dateinamen aus csv-Eingabedateien

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


% import package for visualisation
import VisualiseFeMeshAndData.*


% -- Create folders to save figures ------------------------------------- %

if flag.saveFigures == 1
    import BasicUtilities.CreateFolder
    CreateFolder(folder.outputDataFolder,'pdf/');

    if flag.createFigFiles == 1
        CreateFolder(folder.outputDataFolder,'fig/');
    end

end

% ----------------------------------------------------------------------- %


% -- Load node coordinates and connectivity ----------------------------- %

nodes        = loadNodeCoordinates(folder.inputDataFolder);
inziConnect  = loadConnectivity(   folder.inputDataFolder);

% ----------------------------------------------------------------------- %


% -- Show fe mesh ------------------------------------------------------- %

controlStructMesh.figPos             = [22, 686, 560, 420];
controlStructMesh.flagSaveFigures    = flag.saveFigures;
controlStructMesh.flagFigVisible     = flag.figVisible;
controlStructMesh.flagCreateFigFiles = flag.createFigFiles;
controlStructMesh.savePath           = folder.outputDataFolder;

ShowFiniteElementMesh(nodes, inziConnect, controlStructMesh);

% ----------------------------------------------------------------------- %


% -- Show connectivity matrix ------------------------------------------- %

controlStructMatConnect.figPos             = [22, 183, 560, 420];
controlStructMatConnect.flagSaveFigures    = flag.saveFigures;
controlStructMatConnect.flagFigVisible     = flag.figVisible;
controlStructMatConnect.flagCreateFigFiles = flag.createFigFiles;
controlStructMatConnect.savePath           = folder.outputDataFolder;

ShowNodeConnectivity(nodes, inziConnect, controlStructMatConnect);

% ----------------------------------------------------------------------- %


% -- Show element IDs --------------------------------------------------- %

elementIDStruct.label.title        = 'element ID distribution';
elementIDStruct.label.x            = 'x [m]';
elementIDStruct.label.y            = 'y [m]';
elementIDStruct.label.cbar         = 'elementID';
elementIDStruct.figPos             = [590, 900, 560, 420];
elementIDStruct.flagSaveFigures    = flag.saveFigures;
elementIDStruct.flagFigVisible     = flag.figVisible;
elementIDStruct.flagCreateFigFiles = flag.createFigFiles;
elementIDStruct.savePath           = folder.outputDataFolder;
elementIDStruct.saveName           = 'elementID';

elementTable = loadElementTable(folder.inputDataFolder);

ShowElementDataDistribution(nodes, inziConnect, elementTable(:,1), elementIDStruct);

% ----------------------------------------------------------------------- %


% -- Show material data IDs --------------------------------------------- %

matIDStruct.label.title        = 'material data set ID distribution';
matIDStruct.label.x            = 'x [m]';
matIDStruct.label.y            = 'y [m]';
matIDStruct.label.cbar         = 'matID';
matIDStruct.figPos             = [590, 660, 560, 420];
matIDStruct.flagSaveFigures    = flag.saveFigures;
matIDStruct.flagFigVisible     = flag.figVisible;
matIDStruct.flagCreateFigFiles = flag.createFigFiles;
matIDStruct.savePath           = folder.outputDataFolder;
matIDStruct.saveName           = 'matID';

ShowElementDataDistribution(nodes, inziConnect, elementTable(:,2), matIDStruct);

% ----------------------------------------------------------------------- %


% -- Show load data IDs ------------------------------------------------- %

loadIDStruct.label.title        = 'load data set ID distribution';
loadIDStruct.label.x            = 'x [m]';
loadIDStruct.label.y            = 'y [m]';
loadIDStruct.label.cbar         = 'loadID';
loadIDStruct.figPos             = [590, 633, 560, 420];
loadIDStruct.flagSaveFigures    = flag.saveFigures;
loadIDStruct.flagFigVisible     = flag.figVisible;
loadIDStruct.flagCreateFigFiles = flag.createFigFiles;
loadIDStruct.savePath           = folder.outputDataFolder;
loadIDStruct.saveName           = 'loadID';

ShowElementDataDistribution(nodes, inziConnect, elementTable(:,3), loadIDStruct );

% ----------------------------------------------------------------------- %


% -- Show Dirichlet bc -------------------------------------------------- %

dirBCStruct.label.title        = 'dirichlet bc';
dirBCStruct.label.x            = 'x [m]';
dirBCStruct.label.y            = 'y [m]';
dirBCStruct.figPos             = [1160, 686, 560, 420];
dirBCStruct.flagSaveFigures    = flag.saveFigures;
dirBCStruct.flagFigVisible     = flag.figVisible;
dirBCStruct.flagCreateFigFiles = flag.createFigFiles;
dirBCStruct.savePath           = folder.outputDataFolder;
dirBCStruct.saveName           = 'condBoundDir';

condBoundDir = loadCondBoundDir(folder.inputDataFolder);

if ~isempty(condBoundDir)
    ShowNodeDataDistribution(nodes, inziConnect, condBoundDir, dirBCStruct);
end


% ----------------------------------------------------------------------- %


% -- Show initial condition --------------------------------------------- %

condInitialStruct.label.title        = 'initial condition';
condInitialStruct.label.x            = 'x [m]';
condInitialStruct.label.y            = 'y [m]';
condInitialStruct.figPos             = [590, 133, 560, 420];
condInitialStruct.flagSaveFigures    = flag.saveFigures;
condInitialStruct.flagFigVisible     = flag.figVisible;
condInitialStruct.flagCreateFigFiles = flag.createFigFiles;
condInitialStruct.savePath           = folder.outputDataFolder;
condInitialStruct.saveName           = 'condInitial';

condInitial  = loadCondInitial(folder.inputDataFolder);

if ~isempty(condInitial)
    ShowNodeDataDistribution(nodes, inziConnect, condInitial, condInitialStruct);
end

% ----------------------------------------------------------------------- %


% -- Show movable nodes -------------------------------------------- %

movNodesStruct.label.title        = 'movable nodes';
movNodesStruct.label.x            = 'x [m]';
movNodesStruct.label.y            = 'y [m]';
movNodesStruct.figPos             = [590, 133, 560, 420];
movNodesStruct.flagSaveFigures    = flag.saveFigures;
movNodesStruct.flagFigVisible     = 'on'; %flag.figVisible;
movNodesStruct.flagCreateFigFiles = flag.createFigFiles;
movNodesStruct.savePath           = folder.outputDataFolder;
movNodesStruct.saveName           = 'movMesh_Nodes';


movNodes = loadMovNodes(folder.inputDataFolder);

if ~isempty(movNodes)
    ShowMeshMovNodeData('MovNodes', nodes, inziConnect, movNodes, movNodesStruct);
    pause
end

% ----------------------------------------------------------------------- %


% -- Show movable boundary nodes -------------------------------------------- %

movNodesBoundStruct.label.title        = 'movable boundary nodes';
movNodesBoundStruct.label.x            = 'x [m]';
movNodesBoundStruct.label.y            = 'y [m]';
movNodesBoundStruct.figPos             = [590, 133, 560, 420];
movNodesBoundStruct.flagSaveFigures    = flag.saveFigures;
movNodesBoundStruct.flagFigVisible     = 'on'; %flag.figVisible;
movNodesBoundStruct.flagCreateFigFiles = flag.createFigFiles;
movNodesBoundStruct.savePath           = folder.outputDataFolder;
movNodesBoundStruct.saveName           = 'movMesh_NodesBound';


movNodesBound = loadMovNodesBound(folder.inputDataFolder);

if ~isempty(movNodesBound)
    ShowMeshMovNodeData('MovNodesBound', nodes, inziConnect, movNodesBound, movNodesBoundStruct);
    pause
end

% ----------------------------------------------------------------------- %


% -- Show interior SSL nodes -------------------------------------------- %

nodesSSLIntStruct.label.title        = 'interior nodes of shear slip layer';
nodesSSLIntStruct.label.x            = 'x [m]';
nodesSSLIntStruct.label.y            = 'y [m]';
nodesSSLIntStruct.figPos             = [590, 133, 560, 420];
nodesSSLIntStruct.flagSaveFigures    = flag.saveFigures;
nodesSSLIntStruct.flagFigVisible     = 'on'; %flag.figVisible;
nodesSSLIntStruct.flagCreateFigFiles = flag.createFigFiles;
nodesSSLIntStruct.savePath           = folder.outputDataFolder;
nodesSSLIntStruct.saveName           = 'movMesh_SSLNodesInt';


nodesSSLInt = loadNodesSSLInt(folder.inputDataFolder);

if ~isempty(nodesSSLInt)
    ShowMeshMovNodeData('SSLNodesInt', nodes, inziConnect, nodesSSLInt, nodesSSLIntStruct);
    pause
end

% ----------------------------------------------------------------------- %


% -- Show SSL mesh---------- -------------------------------------------- %

inziConnectSSLStruct.label.title        = 'shear slip layer elements';
inziConnectSSLStruct.label.x            = 'x [m]';
inziConnectSSLStruct.label.y            = 'y [m]';
inziConnectSSLStruct.figPos             = [590, 133, 560, 420];
inziConnectSSLStruct.flagSaveFigures    = flag.saveFigures;
inziConnectSSLStruct.flagFigVisible     = flag.figVisible;
inziConnectSSLStruct.flagCreateFigFiles = flag.createFigFiles;
inziConnectSSLStruct.savePath           = folder.outputDataFolder;
inziConnectSSLStruct.saveName           = 'movMesh_SSL';

inziConnectSSL = loadConnectivitySSL(folder.inputDataFolder);

if ~isempty(inziConnectSSL)
    ShowMeshMovElementData('SSLElements', nodes, inziConnect, inziConnectSSL, inziConnectSSLStruct);
end

% ----------------------------------------------------------------------- %


% -- Show SSL elemet ordering ------------------------------------------- %

eleSSLStruct.label.title        = 'ordering of shear slip layer elements';
eleSSLStruct.label.x            = 'x [m]';
eleSSLStruct.label.y            = 'y [m]';
eleSSLStruct.figPos             = [590, 133, 560, 420];
eleSSLStruct.flagSaveFigures    = flag.saveFigures;
eleSSLStruct.flagFigVisible     = 'on'; %flag.figVisible;
eleSSLStruct.flagCreateFigFiles = flag.createFigFiles;
eleSSLStruct.savePath           = folder.outputDataFolder;
eleSSLStruct.saveName           = 'movMesh_SSLOrdering';

inziConnectSSL = loadConnectivitySSL(folder.inputDataFolder);

if ~isempty(inziConnectSSL)
    ShowMeshMovElementData('SSLEleOrdering', nodes, inziConnect, inziConnectSSL, eleSSLStruct);
    pause
end

% ----------------------------------------------------------------------- %


% -- Show SSL connectivity matrix --------------------------------------- %

controlStructConnectSSL.figPos             = [22, 183, 560, 420];
controlStructConnectSSL.flagSaveFigures    = flag.saveFigures;
controlStructConnectSSL.flagFigVisible     = 'on'; %flag.figVisible;
controlStructConnectSSL.flagCreateFigFiles = flag.createFigFiles;
controlStructConnectSSL.savePath           = folder.outputDataFolder;
controlStructConnectSSL.saveName           = 'movMesh_SSLConnectivity';

inziConnectSSL = loadConnectivitySSL(folder.inputDataFolder);

if ~isempty(inziConnectSSL)
    ShowNodeConnectivitySSL(nodes, inziConnect, inziConnectSSL, controlStructConnectSSL);
    pause
end

% ---------------------------------------------------------------------- %


end %function VisualiseCubitDataCsv

% ======================================================================= %


% ======================================================================= %
% help functions
% ======================================================================= %

function [ nodes ] = loadNodeCoordinates(folderInputDataFiles)

inputDataCoordinates  = importdata([folderInputDataFiles,'nodeCoordinates.csv'],',',7);

nodes                 = inputDataCoordinates.data(:,2:end);

end

% ======================================================================= %


function [inziConnect] = loadConnectivity(folderInputDataFiles)

inputDataConnectivity = importdata([folderInputDataFiles,'nodeConnectivity.csv'],',',7);

connectivity          = inputDataConnectivity.data(:,2:end);

inziConnect           = num2cell(connectivity,2);

end

% ======================================================================= %


function [elementTable] = loadElementTable(folderInputDataFiles)
% Function loading element type table.
% Array with nEle rows and
% 1st column: element type ID
% 2nd column: material parameter set ID
% 3rd column: load parameter set ID

    inputDataElementTable = importdata([folderInputDataFiles,'elementTable.csv'],',',7);

    elementTable          = inputDataElementTable.data(:,2:end);

end %function loadElementTable

% ======================================================================= %


function [condBoundDir] = loadCondBoundDir(folderInputDataFiles)

try
    inputDataBCDir = importdata([folderInputDataFiles,'condBoundDir.csv'],',',7);
catch %#ok<CTCH>
    inputDataBCDir.data = [];
end

    condBoundDir = inputDataBCDir.data;

end

% ======================================================================= %


function [condInitial] = loadCondInitial(folderInputDataFiles)

try
    inputDataInitial = importdata([folderInputDataFiles,'condInitial.csv'],',',7);
catch %#ok<CTCH>
    inputDataInitial.data = [];
end

    condInitial = inputDataInitial.data;

end

% ======================================================================= %


function [ nodesSSLInt ] = loadNodesSSLInt(folderInputDataFiles)

try
    inputDataSSLNodesInt = importdata([folderInputDataFiles,'meshMovSSLNodesInt.csv'],',',7);
catch
    inputDataSSLNodesInt.data = [];
end

nodesSSLInt = inputDataSSLNodesInt.data;

end

% ======================================================================= %


function [ movNodes ] = loadMovNodes(folderInputDataFiles)

try
    inputDataMovNodes = importdata([folderInputDataFiles,'meshMovNodes.csv'],',',7);
catch
    inputDataMovNodes.data = [];
end

movNodes = inputDataMovNodes.data;

end

% ======================================================================= %


function [ movNodesBound ] = loadMovNodesBound(folderInputDataFiles)

try
    inputDataMovNodesBound = importdata([folderInputDataFiles,'meshMovNodesBound.csv'],',',7);
catch
    inputDataMovNodesBound.data = [];
end

movNodesBound = inputDataMovNodesBound.data;

end

% ======================================================================= %


function [inziConnectSSL] = loadConnectivitySSL(folderInputDataFiles)

try
    inputDataConnectivity = importdata([folderInputDataFiles,'meshMovSSL.csv'],',',7);

    connectivity          = inputDataConnectivity.data(:,2:end);

    inziConnectSSL        = num2cell(connectivity,2);

catch

    inziConnectSSL = [];

end

end

% ======================================================================= %

% ======================================================================= %
% ======================================================================= %
