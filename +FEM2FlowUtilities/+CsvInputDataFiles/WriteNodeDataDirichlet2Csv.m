function [] = WriteNodeDataDirichlet2Csv(pathToFolder, fileName, nameTestExample, bcDir )
% Function writing Dirichlet data to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% bcDir:           nDirichletNodes x 7 matrix
%                  matrix containing node number with Dirichlet bc data
%                  [ nodeNumb | dofID | value | timeID | timeSlabID | switchID | sysIndexID ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteNodeDataDirichlet2Csv(pathToFolder, fileName, nameTestExample, bcDir )

% created by   : Henning Schippke
% created on   : 21.04.15
% last modified: 21.08.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');

fprintf(fid,'# %s -- dirichlet boundary data \n', nameTestExample );
fprintf(fid,'# data created or modifed using Matlab: %s, %2.0f:%2.0f:%2.0f\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );
fprintf(fid,'# \n');
fprintf(fid,'# number of dirichlet bc: %-6d\n', size(bcDir,1) );
fprintf(fid,'# \n');
fprintf(fid,'# %6s, %5s, %5s, %12s, %10s, %8s, %14s \n\n','nodeID','dofID','value','timeID','timeSlabID','switchID','sysIndexID');
% nb: timeVarSetID -- (verweist auf) --> timeVarFctID (in dataTime.csv)

for ii = 1:size(bcDir,1)  
    fprintf(fid,'%6d, %2d, %-8.4f, %2d, %1d, %1d, %1d \n',bcDir(ii,:));
end

fclose(fid);


end %function

% ======================================================================= %
% ======================================================================= %