function [] = WriteNodeDataIc2Csv(pathToFolder, fileName, nameTestExample, ic)
% Function writing initial condition data to csv FEM2Flow input data file.
%
%
% Input Data:
% --------------
% 
% pathToFolder:    string
%                  path to output data folder
%
% fileName:        string
%                  name of file to be created (including csv-suffix)
%
% nameTestExample: string
%                  name of calculation example; e.g. 'Driven Cavity'
%
% ic:              nDirichletNodes x 3 matrix
%                  matrix containing node number with corresponding dofID
%                  and value
%                  [ nodeNumb | dofID | value ]
%
%
% Output Data:
% ---------------
% []: none
%
%
% Usage:
% ---------
% [] = WriteNodeDataIc2Csv(pathToFolder, fileName, nameTestExample, ic)

% created by   : Henning Schippke
% created on   : 21.04.15
% last modified: 19.05.15

% ToDo:
% ...

% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %


currentDate = date;
currentTime = clock;
currentTime = currentTime(4:6);


fid = fopen([pathToFolder,fileName],'w','n','UTF-8');

fprintf(fid,'# %s -- initial condition data \n', nameTestExample );
fprintf(fid,'# data created or modified using Matlab: %s, %2.0f:%2.0f:%2.0f\n', currentDate, currentTime(1), currentTime(2), currentTime(3) );
fprintf(fid,'# \n');
fprintf(fid,'# number of initial conditions: %-6d\n', size(ic,1) );
fprintf(fid,'# \n');
fprintf(fid,'# %6s, %5s, %5s \n\n','nodeID','dofID','value');

for ii = 1:size(ic,1)
    fprintf(fid,'%6d, %2d, %-8.4f \n', ic(ii,:) );
end

fclose(fid);  


end %function

% ======================================================================= %
% ======================================================================= %