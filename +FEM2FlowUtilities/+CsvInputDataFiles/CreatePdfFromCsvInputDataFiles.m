function [] = CreatePdfFromCsvInputDataFiles(currPath, preProcFlag, varargin)
% Function generating pdf files from fe mesh and element data as well as
% boundary and initial (node) conditions from FEM2Flow csv input data files.
%
% Note: The folder 'MyMatlabFunctions' must be added to the Matlab path
% in order to use this function.
%
% Input data:
% --------------
%
% currPath: string containing the path of the file calling this function (no trailing /)
%
% preProcFlag: string
%              flag specifing which type of csv files will be transleted
%              'cubit': csv files directly after extraction from Cubit
%              'reordered': csv files after rev. Cuthill-McKee reordering
%
% varargin: {1}: flagPreset : structure
%                             with flags specifying pdf/figure creation
%                    .saveFigures    = 0 : no pdf files are saved
%                                      1 :    pdf files are saved (default)
%                    .createFigFiles = 0 : no additional fig files are saved (default)
%                                      1 :    additional fig files are saved
%                    .figVisible     = 0 : created figures are not displayed on screen (default)
%                                    = 1 : created figures are     displayed on screen
%
% Output data:
% ---------------
% [] : None
%
% Usage:
% ---------
% [] = CreatePdfFromCsvInputDataFiles(currPath, preProcFlag, varargin)

% created by:    Henning Schippke
% created on:    05.02.16
% last modified: 05.02.16

% TO DO:
% * ...

% ----------------------------------------------------------------- %
% ----------------------------------------------------------------- %


if strcmp(preProcFlag, 'cubit')
    folder.inputDataFolder  = './FeModelDataModified/';

elseif strcmp(preProcFlag, 'reordered')
    folder.inputDataFolder  = './FeModelInputData/';

end


folder.outputDataFolder = './Visualisation/';


flag.saveFigures    = 1;
flag.createFigFiles = 0;
flag.figVisible     = 'off';

if length(varargin) > 0
    flagPreset = varargin{1};

    if ~isfield(flagPreset, 'saveFigures')
    flag.saveFigures    = flagPreset.saveFigures;
    end

    if ~isfield(flagPreset, 'createFigFiles')
        flag.createFigFiles = flagPreset.createFigFiles;
    end

    if ~isfield(flagPreset, 'figVisible')
        flag.figVisible     = flagPreset.figVisible;
    end

end



display('... generating pdf files displaying input data');
FEM2FlowUtilities.CsvInputDataFiles.VisualiseCsvInputData(folder, flag);

if  strcmp(flag.figVisible,'on')
    pause(); close all
end


end %function