function [] = VisualiseFeModelData( nameCalcExample, pathOfCallingScript, visCase, varargin )
% Function visualising the csv data files extracted from Cubit fe
% model without changing the Cubit node connectivity.
%
% Furthermore, vtk-files (*.vtu) are generated which gives the possibilty
% to check the extracted input data via Paraview.
%
%
% InputData:
% -------------
%
% nameCalcExample: string specifying name of calculation example
%
% pathOfCallingScript: string specifying (absolute) path of calling script
%                      (without trailing slash '/')
%
% visCase: string specifying visualisation case
%          'cubit':     csv files directly after extraction from Cubit
%          'reordered': csv files after rev. Cuthill-McKee reordering
%
% optional input arguments (key,value):
% * 'createVtkFiles': 0 or 1, specify if vtk-files will be created
%                     default: 1 (= yes)
% * 'executeCreateVtkFilesParallel': 0 or 1, specify if creation of vtk files
%                                    will be performed in parallel using Matlabs
%                                    Parallel Toolbox (parpool or matlabpool)
%                                    default: 1 (= yes)
%
%
% OutputData:
% --------------
% []: None
%
%
% Usage:
% ---------
%
% VisualiseFeModelData( nameCalcExample, pathOfCallingScript, visCase, 'optionalInputArgument', correspondingValue )


% created by   : Henning Schippke
% created on   : 24.01.17 (created originally as script on 23.03.15)
% last modified: 26.01.17

% ToDo:
% - Export vtk: Alternativ in python schreiben

% ======================================================================= %
% ======================================================================= %

% -------------------------------------------------------------------
% Optional input arguments
% -------------------------------------------------------------------

% createVtkFiles
defaultCreateVtkFiles = 1;
checkCreateVtkFiles   = @isnumeric;

% executeCreateVtkFilesParallel
defaultExecuteCreateVtkFilesParallel = 1;
checkExecuteCreateVtkFilesParallel   = @isnumeric;


p = inputParser;

p.addRequired('nameCalcExample');
p.addRequired('pathOfCallingScript');
p.addRequired('visCase');

p.addParamValue('createVtkFiles'               , defaultCreateVtkFiles               , checkCreateVtkFiles               );
p.addParamValue('executeCreateVtkFilesParallel', defaultExecuteCreateVtkFilesParallel, checkExecuteCreateVtkFilesParallel);


p.parse(nameCalcExample, pathOfCallingScript, visCase, varargin{:});

createVtkFiles                = p.Results.createVtkFiles;
executeCreateVtkFilesParallel = p.Results.executeCreateVtkFilesParallel;

% -------------------------------------------------------------------
% end | Optional input arguments
% -------------------------------------------------------------------


% !!! Path to "MyMatlabFunctions" must be added to Matlab-Path prior to using this function !!!


% -- Visualise Cubit csv data --------------------------------------------- %

FEM2FlowUtilities.CsvInputDataFiles.CreatePdfFromCsvInputDataFiles(pathOfCallingScript, visCase)

% ------------------------------------------------------------------------- %


% -- Export csv input data to vtk ----------------------------------------- %

if createVtkFiles == 1
    FEM2FlowUtilities.CsvInputDataFiles.CreateVtkFromCsvInputDataFiles(pathOfCallingScript, visCase, 'parallel', executeCreateVtkFilesParallel);
end
% ----------------------------------------------------------------------- %


end %function

% ======================================================================= %
% ======================================================================= %
